mod client;
mod model;

pub use self::client::Client;
pub use self::model::Comic;
pub use self::model::ComicImages;
pub use self::model::Image;
pub use self::model::ImageKind;
pub use self::model::TagKind;

/// Library error type
#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("http error")]
    Reqwest(#[from] reqwest::Error),

    #[error("tokio join error")]
    TokioJoin(#[from] tokio::task::JoinError),

    #[error("missing metadata")]
    MissingMetadata,

    #[error("json error")]
    Json(#[from] serde_json::Error),
}

#[cfg(test)]
mod test {
    use super::*;
    use std::num::NonZeroU64;

    const ID_1: NonZeroU64 = NonZeroU64::new(177013).unwrap();

    #[tokio::test]
    async fn it_works() {
        let client = Client::new();
        let comic = client.get_comic(ID_1).await.expect("failed to get comic");
        dbg!(comic);
    }
}
