use std::collections::HashMap;
use std::num::NonZeroU64;
use time::OffsetDateTime;

/// Comic metadata
#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct Comic {
    pub id: NonZeroU64,
    pub media_id: Box<str>,
    pub title: Title,
    pub images: Images,
    pub scanlator: Box<str>,
    #[serde(with = "time::serde::timestamp")]
    pub upload_date: OffsetDateTime,
    pub tags: Vec<Tag>,
    pub num_pages: u32,

    #[serde(flatten)]
    pub unknown: HashMap<String, serde_json::Value>,
}

/// Comic title metadata
#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct Title {
    /// English title
    pub english: Box<str>,

    /// Japanese title
    pub japanese: Option<Box<str>>,

    /// Pretty title
    pub pretty: Box<str>,

    /// Unknown titles
    #[serde(flatten)]
    pub unknown: HashMap<String, serde_json::Value>,
}

/// Comic images metadata
#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub struct Images {
    /// This may be a sparse array/object
    pub pages: serde_json::Value,

    pub cover: Image,
    pub thumbnail: Image,
    #[serde(flatten)]
    pub unknown: HashMap<String, serde_json::Value>,
}

/// Image data
#[derive(Clone, Debug, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub struct Image {
    /// Image height
    #[serde(rename = "h")]
    pub height: u32,

    /// Image width
    #[serde(rename = "w")]
    pub width: u32,

    /// Image kind
    #[serde(rename = "t")]
    pub kind: ImageKind,

    /// Unknown data
    #[serde(flatten)]
    pub unknown: HashMap<String, serde_json::Value>,
}

/// The image kind
#[derive(Copy, Clone, Debug, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub enum ImageKind {
    #[serde(rename = "j")]
    Jpeg,

    #[serde(rename = "p")]
    Png,

    #[serde(rename = "g")]
    Gif,

    #[serde(rename = "w")]
    Webp,
}

impl ImageKind {
    /// Get the extension as a string.
    ///
    /// Does not have a leading "."
    pub fn get_ext(self) -> &'static str {
        match self {
            Self::Jpeg => "jpg",
            Self::Png => "png",
            Self::Gif => "gif",
            Self::Webp => "webp",
        }
    }
}

/// Comic tag metadata
#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub struct Tag {
    // This is NOT the nhentai id.
    pub id: u64,
    /// This is the nhentai id.
    pub nh_id: Box<str>,
    #[serde(rename = "type")]
    pub kind: TagKind,
    pub name: Box<str>,
    pub url: Box<str>,
    pub created_at: Box<str>,
    pub updated_at: Box<str>,
    pub deleted_at: Option<Box<str>>,
    pub pivot: TagPivot,

    #[serde(flatten)]
    pub unknown: HashMap<String, serde_json::Value>,
}

/// The kind of tag
#[derive(Clone, Debug, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub enum TagKind {
    #[serde(rename = "language")]
    Language,

    #[serde(rename = "parody")]
    Parody,

    #[serde(rename = "character")]
    Character,

    #[serde(rename = "tag")]
    Tag,

    #[serde(rename = "group")]
    Group,

    #[serde(rename = "artist")]
    Artist,

    #[serde(rename = "category")]
    Category,
}

/// Tag pivot metadata
#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub struct TagPivot {
    pub book_id: u64,
    pub tag_id: u64,

    #[serde(flatten)]
    pub unknown: HashMap<String, serde_json::Value>,
}
