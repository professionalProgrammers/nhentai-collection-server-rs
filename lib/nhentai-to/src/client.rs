use crate::Comic;
pub use crate::Error;
use once_cell::sync::Lazy;
use regex::Regex;
use reqwest::header::HeaderMap;
use reqwest::header::HeaderValue;
use reqwest::header::ACCEPT;
use reqwest::header::REFERER;
use std::num::NonZeroU64;

const USER_AGENT_VALUE: &str = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/134.0.0.0 Safari/537.36";

static ACCEPT_VALUE: HeaderValue = HeaderValue::from_static("*/*");
static REFERER_VALUE: HeaderValue = HeaderValue::from_static("https://nhentai.to/");

/// The client
#[derive(Debug, Clone)]
pub struct Client {
    /// The inner http client
    pub client: reqwest::Client,
}

impl Client {
    /// Make a new client
    pub fn new() -> Self {
        let mut headers = HeaderMap::new();
        headers.insert(ACCEPT, ACCEPT_VALUE.clone());
        headers.insert(REFERER, REFERER_VALUE.clone());

        let client = reqwest::Client::builder()
            .user_agent(USER_AGENT_VALUE)
            .default_headers(headers)
            .build()
            .expect("failed to build client");

        Self { client }
    }

    /// Get a comic by id
    pub async fn get_comic(&self, id: NonZeroU64) -> Result<Comic, Error> {
        static METADATA_REGEX: Lazy<Regex> = Lazy::new(|| {
            Regex::new(
                "(?s)var reader = new N\\.reader\\(.*gallery: (.*?),\\s*start_page: 1.*\\}\\)",
            )
            .unwrap()
        });

        // Avoid sparse arrays for image pages which can occur on the main page.
        // Also lets us avoid hacking around a trailing comma which only appears on the main page.
        let url = format!("https://nhentai.to/g/{id}/1");

        let text = self
            .client
            .get(url)
            .send()
            .await?
            .error_for_status()?
            .text()
            .await?;

        let comic = tokio::task::spawn_blocking(move || {
            let captures = METADATA_REGEX.captures(&text);
            let metadata = captures
                .and_then(|captures| captures.get(1))
                .ok_or(Error::MissingMetadata)?
                .as_str();
            let mut comic: Comic = serde_json::from_str(metadata)?;

            // The id is always wrong, fix it
            comic.id = id;

            Ok::<_, Error>(comic)
        })
        .await??;

        Ok(comic)
    }
}

impl Default for Client {
    fn default() -> Self {
        Self::new()
    }
}
