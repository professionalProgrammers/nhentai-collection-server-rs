#[cfg(feature = "reqwest")]
pub use reqwest;

#[derive(Debug, serde::Deserialize)]
pub struct Response {
    pub status: Box<str>,
    pub message: Box<str>,
    pub solution: ResponseSolution,
    #[serde(rename = "startTimestamp")]
    pub start_timestamp: u64,
    #[serde(rename = "endTimestamp")]
    pub end_timestamp: u64,
    pub version: Box<str>,
}

#[derive(Debug, serde::Deserialize)]
pub struct ResponseSolution {
    pub url: Box<str>,
    pub cookies: Vec<ResponseCookie>,
    #[serde(rename = "userAgent")]
    pub user_agent: Box<str>,
}

#[derive(Debug, serde::Deserialize)]
pub struct ResponseCookie {
    pub name: Box<str>,
    pub value: Box<str>,
    pub domain: Box<str>,
}

#[cfg(feature = "reqwest")]
#[derive(Debug, serde::Serialize)]
struct RequestBody<'a> {
    cmd: &'static str,
    url: &'a str,
    #[serde(rename = "maxTimeout")]
    max_timeout: u32,
}

#[cfg(feature = "reqwest")]
pub async fn get(
    client: &reqwest::Client,
    endpoint: &str,
    url: &str,
) -> Result<Response, reqwest::Error> {
    let body = RequestBody {
        cmd: "request.get",
        url,
        max_timeout: 60_000,
    };

    client
        .post(endpoint)
        .json(&body)
        .send()
        .await?
        .error_for_status()?
        .json()
        .await
}
