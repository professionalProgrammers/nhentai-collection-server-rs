use std::marker::PhantomData;

struct Select;
struct From;
struct Join;
struct Where;

struct SqlBuilder<T> {
    out: String,

    _state: PhantomData<T>,
}

impl SqlBuilder<()> {
    pub fn with_capacity(capacity: usize) -> SqlBuilder<()> {
        Self {
            out: String::with_capacity(capacity),
            _state: PhantomData,
        }
    }

    /// Start a select
    pub fn select(mut self) -> SqlBuilder<Select> {
        self.out += "SELECT\n";

        SqlBuilder {
            out: self.out,
            _state: PhantomData,
        }
    }
}

impl SqlBuilder<Select> {
    pub fn column(mut self, column: &str) -> SqlBuilder<Select> {
        self.out += "    ";
        self.out += column;
        self.out += ",\n";

        SqlBuilder {
            out: self.out,
            _state: PhantomData,
        }
    }

    pub fn from(mut self, table: &str) -> SqlBuilder<From> {
        // Strip ",\n"
        self.out.pop();
        self.out.pop();

        self.out += "\n";
        self.out += "FROM\n";
        self.out += "    ";
        self.out += table;

        SqlBuilder {
            out: self.out,
            _state: PhantomData,
        }
    }
}

impl SqlBuilder<From> {
    pub fn join(mut self, table: &str, on_expr: &str) -> SqlBuilder<Join> {
        self.out += "\n";
        self.out += "JOIN\n";
        self.out += "    ";
        self.out += table;
        self.out += "\nON\n";
        self.out += "    ";
        self.out += on_expr;

        SqlBuilder {
            out: self.out,
            _state: PhantomData,
        }
    }
}

impl SqlBuilder<Join> {
    pub fn where_(mut self, expr: &str) -> SqlBuilder<Where> {
        self.out += "\n";
        self.out += "WHERE\n";
        self.out += "    ";
        self.out += expr;

        SqlBuilder {
            out: self.out,
            _state: PhantomData,
        }
    }
}