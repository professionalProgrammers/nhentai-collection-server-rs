SELECT
    name,
    num_comics,
    total_comics,
    (CAST(num_comics as REAL) / CAST(total_comics as REAL)) as percent
FROM
    (
        SELECT
            name,
            (
                SELECT
                    COUNT(comic_id)
                FROM
                    comic_tags
                WHERE
                    tag_id = tags.id
            ) as num_comics,
            (
                SELECT
                    COUNT(id)
                FROM
                    comics
            ) as total_comics
        FROM
            tags
    )
ORDER BY
    num_comics DESC
LIMIT
    100;