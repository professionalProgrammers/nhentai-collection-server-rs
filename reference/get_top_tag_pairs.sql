WITH comic_subset AS (
    SELECT
        id
    FROM
        comics
    WHERE
        EXISTS (
            SELECT
                comic_id
            FROM
                comic_tags
            WHERE
                comic_tags.comic_id = comics.id AND
                EXISTS (
                    SELECT
                        id
                    FROM
                        tags
                    WHERE
                        tags.id = comic_tags.tag_id AND
                        tags.name = 'english'
                )
        )
), tag_count AS (
    SELECT
        id,
        name,
        type,
        (
            SELECT
                COUNT(comic_id)
            FROM
                comic_tags
            JOIN
                comic_subset
            ON 
                comic_tags.comic_id = comic_subset.id
            WHERE
                tag_id = tags.id
        ) AS num_comics
    FROM
        tags
    WHERE
        EXISTS (
            SELECT
                id
            FROM
                comic_tags
            JOIN
                comic_subset
            ON
                comic_tags.comic_id = comic_subset.id
            WHERE 
                comic_tags.tag_id = tags.id
        )
    ORDER BY
        num_comics DESC
), total_comics AS (
    SELECT
        COUNT(id) as total_comics
    FROM
        comic_subset
)
SELECT
    tags1.name AS tag1_name,
    tags1.type AS tag1_type,
    tags1.num_comics AS tag1_num_comics,
    (CAST(tags1.num_comics AS REAL) / CAST(total_comics as REAL)) AS tag1_percent,
    
    tags2.name AS tag2_name,
    tags2.type AS tag2_type,
    tags2.num_comics AS tag2_num_comics,
    (CAST(tags2.num_comics AS REAL) / CAST(total_comics AS REAL)) AS tag2_percent,
    
    (tags1.num_comics + tags2.num_comics) AS score
FROM
    tag_count AS tags1,
    total_comics
JOIN
    tag_count AS tags2
ON
    tags1.id != tags2.id AND
    tags1.id < tags2.id
WHERE
    -- ((tag1_percent > 0.1 AND tag1_percent < 0.90) AND (tag2_percent > 0.1 AND tag2_percent < 0.90)) AND
    ((tag1_name NOT IN ('translated', 'english') AND tag2_name NOT IN ('translated', 'english')))
ORDER BY
    score DESC
LIMIT 
    10;