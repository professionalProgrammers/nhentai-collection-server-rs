mod api_handler;
mod cli_options;
pub mod collection_manager;
mod config;
mod routes;
mod util;

use self::cli_options::CliOptions;
use self::collection_manager::CollectionManager;
use self::config::Config;
use self::util::wait_for_shutdown_signal;
use anyhow::bail;
use anyhow::Context;
use axum_server::tls_rustls::RustlsConfig;
use std::sync::Arc;
use std::time::Duration;
use tracing::error;
use tracing::info;

/// The entry
fn main() -> anyhow::Result<()> {
    // This MUST run first.
    // This is because it may error out and exit early,
    // and it will NOT run destructors if it does so.
    let cli_options: CliOptions = argh::from_env();

    let config = Config::load(cli_options.config).context("failed to load config")?;
    tracing_subscriber::fmt()
        .try_init()
        .ok()
        .context("failed to register logger")?;

    let tokio_rt = tokio::runtime::Builder::new_multi_thread()
        .enable_all()
        .build()
        .context("failed to build tokio runtime")?;

    tokio_rt.block_on(async_main(config))
}

/// The "async main" of the program.
async fn async_main(config: Config) -> anyhow::Result<()> {
    let config = Arc::new(config);
    let collection_manager = CollectionManager::new(config.clone())
        .await
        .context("failed to create collection manager")?;

    // Validate collection.
    // If there is an error, consider adding repair options.
    // TODO: How could this fail in a way that is impossible to repair automatically?
    // If we error here, everything else will probably fail so this is fatal.
    collection_manager
        .indexer
        .validate_all()
        .await
        .context("failed to index collection")?;

    let routes = self::routes::routes(
        config.collection_directory.clone(),
        config.public_directory.clone(),
        collection_manager.clone(),
    );

    let server_handle = axum_server::Handle::new();
    let server_task_handle = match config.tls.as_ref() {
        Some(tls_config) => {
            info!("creating a TLS server...");

            let rustls_config = if tls_config.self_signed {
                info!("generating self-signed certificate...");
                let cert = rcgen::generate_simple_self_signed([])
                    .context("failed to generate self-signed certificate")?;

                let pem_cert = cert.cert.pem();
                let pem_key = cert.key_pair.serialize_pem();

                RustlsConfig::from_pem(pem_cert.into(), pem_key.into()).await?
            } else {
                bail!("TLS without using a self-signed cert is currently unsupported");
            };

            let server = axum_server::bind_rustls(config.address, rustls_config)
                .handle(server_handle.clone())
                .serve(routes.into_make_service());
            tokio::spawn(server)
        }
        None => {
            let server = axum_server::bind(config.address)
                .handle(server_handle.clone())
                .serve(routes.into_make_service());
            tokio::spawn(server)
        }
    };

    let protocol = if config.tls.is_some() {
        "https"
    } else {
        "http"
    };
    let address = config.address;
    info!("starting server on '{protocol}://{address}'");

    wait_for_shutdown_signal()
        .await
        .context("failed to wait for shutdown signal")?;
    server_handle.graceful_shutdown(Some(Duration::from_secs(1)));

    // During shutdown,
    // we will not return on errors as we want to try to shut down as much as possible gracefully.
    info!("shutting down...");

    let join_server_future = async {
        server_task_handle
            .await
            .context("failed to join server task")?
            .context("server failed to run")
    };

    // Don't wrap this result with anyhow, it already is one.
    if let Err(error) = join_server_future.await {
        error!("{error:?}");
    }

    if let Err(error) = collection_manager
        .shutdown()
        .await
        .context("failed to shutdown collection manager")
    {
        error!("{error:?}");
    }

    Ok(())
}
