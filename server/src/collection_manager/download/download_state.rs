use nd_util::ArcAnyhowError;
use std::collections::HashMap;
use std::sync::Arc;
use tracing::error;
use tracing::warn;
use url::Url;

/// A download state update
#[derive(Debug, Clone)]
pub enum DownloadStateUpdate {
    /// The download task got the metadata, or failed doing so.
    ///
    /// If the metadata could not be downloaded,
    /// the entire download is considered is stopped as it is impossible to recover.
    MetadataDownloaded {
        /// The result
        result: Result<Arc<nhentai::Comic>, ArcAnyhowError>,
    },

    /// Failed to add this file to the download list.
    ///
    /// This is not fatal, but will result in some files failing to download.
    InvalidDownloadListEntry { url: Url },

    /// Failed to create the comic dir
    CreateComicDirFail { error: ArcAnyhowError },

    /// The download list was generated
    DownloadListGenerated {
        download_list: Arc<[(Box<str>, Option<Url>)]>,
    },

    /// A file was saved, or failed to be saved.
    FileSaved {
        file_name: Box<str>,
        result: Result<bool, ArcAnyhowError>,
    },
}

/// The inner download state, representing the current state of a download.
#[derive(Debug)]
pub struct InnerDownloadState {
    /// The result of the metadata download.
    pub(super) metadata_result: Option<Result<Arc<nhentai::Comic>, ArcAnyhowError>>,

    /// A list of download list entries that could not be added to the list.
    pub(super) invalid_download_list_entries: Vec<Url>,

    /// If present, it indicates that creating the comic dir failed.
    pub(super) comic_dir_creaction_error: Option<ArcAnyhowError>,

    /// A map of file downloads and their status.
    ///
    /// It is keyed by filename.
    pub(super) file_downloads: HashMap<Box<str>, Option<Result<bool, ArcAnyhowError>>>,
}

impl InnerDownloadState {
    /// Init a default [`InnerDownloadState`].
    pub(super) fn new() -> Self {
        Self {
            metadata_result: None,
            invalid_download_list_entries: Vec::new(),
            comic_dir_creaction_error: None,
            file_downloads: HashMap::new(),
        }
    }

    /// Apply an update to this state.
    ///
    /// If a `MetadataDownloaded` update is supplied twice, only the first is accepted.
    /// If any update is provided without a `MetadataDownloaded` update, it is not accepted.
    /// If a `DownloadListGenerated` update is supplied twice, only the first is accepted.
    /// If a `CreateComicDirFail` update is supplied twice, only the first is accepted.
    /// If a `FileSaved` update is provided without a `DownloadListGenerated` update, it is not accepted.
    /// If a `FileSaved` update is provided with an entry whose file name is not known, it is not accepted.
    /// If a `FileSaved` update is supplied twice, only the first is accepted.
    pub(super) fn apply_update(&mut self, update: &DownloadStateUpdate) {
        match update {
            DownloadStateUpdate::MetadataDownloaded { result } => {
                if self.metadata_result.is_some() {
                    warn!("`MetadataDownloaded` update duplicated");
                    return;
                }

                self.metadata_result = Some(result.clone());
            }
            DownloadStateUpdate::InvalidDownloadListEntry { url } => {
                if self.metadata_result.is_none() {
                    error!("`MetadataDownloaded` update is missing for the `InvalidDownloadListEntry` update");
                    return;
                }

                self.invalid_download_list_entries.push(url.clone());
            }
            DownloadStateUpdate::DownloadListGenerated { download_list } => {
                if self.metadata_result.is_none() {
                    error!("`MetadataDownloaded` update is missing for the `DownloadListGenerated` update");
                    return;
                }
                if !self.file_downloads.is_empty() {
                    warn!("`DownloadListGenerated` update duplicated");
                    return;
                }

                for (file_name, _) in download_list.iter() {
                    let is_duplicate = self
                        .file_downloads
                        .insert(file_name.clone(), None)
                        .is_some();

                    // TODO: This should be an error
                    if is_duplicate {
                        error!("file \"{file_name}\" is duplicated in the download list");
                    }
                }
            }
            DownloadStateUpdate::CreateComicDirFail { error } => {
                if self.metadata_result.is_none() {
                    error!("`MetadataDownloaded` update is missing for the `CreateComicDirFail` update");
                    return;
                }
                if self.comic_dir_creaction_error.is_some() {
                    warn!("`CreateComicDirFail` update duplicated");
                    return;
                }

                self.comic_dir_creaction_error = Some(error.clone());
            }
            DownloadStateUpdate::FileSaved { file_name, result } => {
                if self.metadata_result.is_none() {
                    error!("`MetadataDownloaded` update is missing for the `FileSaved` update");
                    return;
                }
                if self.file_downloads.is_empty() {
                    error!("`DownloadListGenerated` update is missing for the `FileSaved` update");
                    return;
                }

                let entry = match self.file_downloads.get_mut(&**file_name) {
                    Some(entry) => entry,
                    None => {
                        error!("`file_downloads` missing `file_name` entry for \"{file_name}\"");
                        return;
                    }
                };

                if entry.is_some() {
                    warn!("`FileSaved` update was duplicated for file name \"{file_name}\"");
                    return;
                }

                *entry = Some(result.clone());
            }
        }
    }

    /// Get the metadata result
    pub fn get_metadata_result(&self) -> Option<&Result<Arc<nhentai::Comic>, ArcAnyhowError>> {
        self.metadata_result.as_ref()
    }

    /// Get a list of invalid download list entries
    pub fn get_invalid_download_list_entries(&self) -> &[Url] {
        &self.invalid_download_list_entries
    }

    /// Get the comic dir creation error
    pub fn get_comic_dir_creation_error(&self) -> Option<&ArcAnyhowError> {
        self.comic_dir_creaction_error.as_ref()
    }

    /// Get the file downloads
    pub fn get_file_downloads(&self) -> &HashMap<Box<str>, Option<Result<bool, ArcAnyhowError>>> {
        &self.file_downloads
    }
}

impl Default for InnerDownloadState {
    fn default() -> Self {
        Self::new()
    }
}

/// A download state
#[derive(Debug, Clone)]
pub struct DownloadState {
    /// The inner state
    state: Arc<std::sync::Mutex<InnerDownloadState>>,
}

impl DownloadState {
    /// Make a new clonable download state
    pub(super) fn new() -> Self {
        Self {
            state: Arc::new(std::sync::Mutex::new(InnerDownloadState::new())),
        }
    }

    /// Lock the internal state for immutable access.
    ///
    /// Modifying the state through the returned guard is a logic error.
    pub fn lock(&self) -> impl std::ops::Deref<Target = InnerDownloadState> + '_ {
        self.state.lock().unwrap_or_else(|e| e.into_inner())
    }
}

impl bewu_util::StateUpdateChannelState for DownloadState {
    type Update = DownloadStateUpdate;

    fn apply_update(&self, update: &Self::Update) {
        self.state
            .lock()
            .unwrap_or_else(|e| e.into_inner())
            .apply_update(update);
    }
}
