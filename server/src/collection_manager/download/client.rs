use crate::Config;
use anyhow::ensure;
use anyhow::Context;
use reqwest_cookie_store::RawCookie;
use std::num::NonZeroU64;
use std::sync::Arc;
use tracing::error;
use tracing::info;
use tracing::warn;
use url::Url;

/// A wrapper for an nhentai client
#[derive(Clone)]
pub struct Client {
    pub(super) inner: Arc<InnerClient>,
}

impl Client {
    /// Make a new [`Client`]
    pub fn new(config: Arc<Config>) -> anyhow::Result<Self> {
        let client = nhentai::Client::new();
        if !config.cookies.is_empty() {
            let mut cookie_store = client.cookie_store.lock().unwrap();

            for cookie in config.cookies.iter() {
                let cookie = RawCookie::parse(&**cookie)
                    .with_context(|| format!("failed to parse cookie \"{cookie}\""))?;
                let domain = cookie
                    .domain()
                    .with_context(|| format!("cookie \"{cookie}\" missing domain"))?;
                let url = format!("http://{domain}");
                let url = Url::parse(&url)?;
                cookie_store.insert_raw(&cookie, &url)?;
            }
        }

        // TODO: Do NOT raise this beyond 1.
        // No locking is done for flaresolverr accesses, even though it is needed.
        let api_semaphore = Arc::new(tokio::sync::Semaphore::new(1));
        let image_semaphore = Arc::new(tokio::sync::Semaphore::new(8));

        let nhentai_to_client = nhentai_to::Client::new();

        Ok(Self {
            inner: Arc::new(InnerClient {
                client,
                nhentai_to_client,
                config,
                api_semaphore,
                image_semaphore,
            }),
        })
    }

    /// Get the comic by id.
    ///
    /// Uses a semaphore internally to limit concurrent access.
    pub async fn get_comic(&self, id: NonZeroU64) -> anyhow::Result<nhentai::Comic> {
        let _permit = self
            .inner
            .api_semaphore
            .acquire()
            .await
            .context("api semaphore closed")?;
        let result = self
            .inner
            .client
            .get_comic(id)
            .await
            .context("failed to get comic");

        let error = match result {
            Ok(comic) => {
                return Ok(comic);
            }
            Err(error) => error,
        };

        if is_cloudflare_block(&error) {
            warn!("encountered cloudflare block");

            if let Some(flaresolverr) = self.inner.config.flaresolverr.as_ref() {
                let result = self.get_comic_cloudflare_bypass(id, flaresolverr).await;

                match result {
                    Ok(comic) => Ok(comic),
                    Err(bypass_error) => {
                        error!("{bypass_error:?}");

                        Err(error)
                    }
                }
            } else {
                Err(error)
            }
        } else {
            Err(error)
        }
    }

    /// Get the comic by using a flaresolverr server
    pub async fn get_comic_cloudflare_bypass(
        &self,
        id: NonZeroU64,
        flaresolverr: &Url,
    ) -> anyhow::Result<nhentai::Comic> {
        warn!("attempting to bypass flaresolverr server endpoint \"{flaresolverr}\"");

        let response = flaresolverr::get(
            &self.inner.client.client,
            flaresolverr.as_str(),
            "https://nhentai.net",
        )
        .await
        .context("failed to bypass cloudflare")?;

        let user_agent = response.solution.user_agent;
        let cookie = response
            .solution
            .cookies
            .first()
            .context("missing cookie")?;
        ensure!(&*cookie.name == "cf_clearance");
        let cf_clearance =
            format!("{}={}; Domain={}", cookie.name, cookie.value, cookie.domain).into();

        let bypass_data = CloudflareBypassData {
            user_agent,
            cf_clearance,
        };

        info!(
            "got bypass data user_agent=\"{}\" and cf_clearance={}",
            bypass_data.user_agent, bypass_data.cf_clearance
        );

        *self
            .inner
            .client
            .user_agent
            .lock()
            .unwrap_or_else(|error| error.into_inner()) = bypass_data.user_agent.into();

        // TODO: If this fails, should we restore the old user agent value?
        async {
            let mut cookie_store = self.inner.client.cookie_store.lock().unwrap();
            let cookie = RawCookie::parse(&*bypass_data.cf_clearance).with_context(|| {
                format!("failed to parse cookie \"{}\"", bypass_data.cf_clearance)
            })?;
            let domain = cookie
                .domain()
                .with_context(|| format!("cookie \"{cookie}\" missing domain"))?;
            let url = format!("http://{domain}");
            let url = Url::parse(&url)?;
            cookie_store.insert_raw(&cookie, &url)?;

            anyhow::Ok(())
        }
        .await
        .context("failed to set cf_clearance cookie")?;

        self.inner
            .client
            .get_comic(id)
            .await
            .context("failed to get comic")
    }

    /// Get a comic by id from nhentai.to.
    pub async fn get_comic_nhentai_to(&self, id: NonZeroU64) -> anyhow::Result<nhentai_to::Comic> {
        let comic = self.inner.nhentai_to_client.get_comic(id).await?;
        Ok(comic)
    }
}

pub(super) struct InnerClient {
    /// The nhentai api client
    pub(super) client: nhentai::Client,
    /// A client for nhentai.to
    pub(super) nhentai_to_client: nhentai_to::Client,

    /// The server config
    config: Arc<Config>,

    /// Concurrency limit for api calls
    api_semaphore: Arc<tokio::sync::Semaphore>,
    /// Concurrency limit for image downloads
    pub(super) image_semaphore: Arc<tokio::sync::Semaphore>,
}

/// Check if a given error is a cloudflare block error.
fn is_cloudflare_block(error: &anyhow::Error) -> bool {
    let reqwest_error = error
        .chain()
        .last()
        .and_then(|error| error.downcast_ref::<reqwest::Error>());
    let status_code = reqwest_error.and_then(|error| error.status());

    status_code == Some(reqwest::StatusCode::FORBIDDEN)
}

#[derive(Debug)]
struct CloudflareBypassData {
    user_agent: Box<str>,
    cf_clearance: Box<str>,
}
