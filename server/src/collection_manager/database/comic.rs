use super::Comic;
use super::ComicImages;
use super::DatabaseTask;
use super::Image;
use super::ImageType;
use super::Tag;
use anyhow::Context;
use nd_async_rusqlite::rusqlite::named_params;
use nd_async_rusqlite::rusqlite::Error as RusqliteError;
use nd_async_rusqlite::rusqlite::OptionalExtension;
use std::num::NonZeroU64;

// Upsert
const UPSERT_COMIC_SQL: &str =
    include_str!(concat!(env!("CARGO_MANIFEST_DIR"), "/sql/upsert_comic.sql"));
const UPSERT_IMAGE_SQL: &str =
    include_str!(concat!(env!("CARGO_MANIFEST_DIR"), "/sql/upsert_image.sql"));
const UPSERT_TAG_SQL: &str =
    include_str!(concat!(env!("CARGO_MANIFEST_DIR"), "/sql/upsert_tag.sql"));
const INSERT_COMIC_TAG: &str = include_str!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/sql/insert_comic_tag.sql"
));
pub const DELETE_COMIC_TAGS: &str = include_str!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/sql/delete_comic_tags.sql"
));

// Get
pub(crate) const GET_COMIC_SQL: &str =
    include_str!(concat!(env!("CARGO_MANIFEST_DIR"), "/sql/get_comic.sql"));
pub(crate) const GET_COMIC_IMAGES_SQL: &str = include_str!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/sql/get_comic_images.sql"
));
pub(crate) const GET_COMIC_TAGS_SQL: &str = include_str!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/sql/get_comic_tags.sql"
));

// Delete
const DELETE_COMIC_IMAGES_SQL: &str = include_str!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/sql/delete_comic_images.sql"
));
const DELETE_COMIC_SQL: &str =
    include_str!(concat!(env!("CARGO_MANIFEST_DIR"), "/sql/delete_comic.sql"));

impl DatabaseTask {
    /// Upsert a single comic.
    ///
    /// This will also upsert comic images, tags, and comic tags.
    pub async fn upsert_comic(
        &self,
        comic: Comic,
        comic_images: ComicImages,
        comic_tags: Vec<Tag>,
    ) -> anyhow::Result<()> {
        self.database
            .write(move |database| {
                let transaction = database.transaction()?;

                // Upsert comic metadata
                {
                    let mut statement = transaction.prepare_cached(UPSERT_COMIC_SQL)?;
                    statement
                        .execute(named_params! {
                            ":id": comic.id,
                            ":pretty_title": comic.pretty_title,
                            ":english_title": comic.english_title,
                            ":japanese_title": comic.japanese_title,
                            ":num_favorites": comic.num_favorites,
                            ":upload_date": comic.upload_date.unix_timestamp(),
                        })
                        .context("failed to upsert comic entry")?;
                }

                // Upsert images
                {
                    let mut statement = transaction.prepare_cached(UPSERT_IMAGE_SQL)?;
                    statement
                        .execute(named_params! {
                            ":comic_id": comic.id,
                            ":image_type": ImageType::Cover,
                            ":width": comic_images.cover.width,
                            ":height": comic_images.cover.height,
                            ":extension": comic_images.cover.extension,
                            ":page_number": 0,
                        })
                        .context("failed to upsert cover")?;

                    for (page_number, page) in comic_images.pages.iter().enumerate() {
                        statement
                            .execute(named_params! {
                                ":comic_id": comic.id,
                                ":image_type": ImageType::Page,
                                ":width": page.width,
                                ":height": page.height,
                                ":extension": page.extension,
                                ":page_number": page_number + 1,
                            })
                            .context("failed to upsert page")?;
                    }

                    statement
                        .execute(named_params! {
                            ":comic_id": comic.id,
                            ":image_type": ImageType::Thumbnail,
                            ":width": comic_images.thumbnail.width,
                            ":height": comic_images.thumbnail.height,
                            ":extension": comic_images.thumbnail.extension,
                            ":page_number": 0
                        })
                        .context("failed to upsert thumbnail")?;
                }

                // Upsert tags
                {
                    let mut statement = transaction.prepare_cached(UPSERT_TAG_SQL)?;
                    for tag in comic_tags.iter() {
                        statement
                            .execute(named_params! {
                                ":id": tag.id,
                                ":name": tag.name,
                                ":type": tag.kind,
                            })
                            .with_context(|| {
                                format!(
                                    "failed to upsert tag entry (tag_id={}, tag_name={})",
                                    tag.id, tag.name
                                )
                            })?;
                    }
                }

                // Remove old comic_tags links
                {
                    let mut statement = transaction.prepare_cached(DELETE_COMIC_TAGS)?;
                    statement
                        .execute(named_params! {
                            ":id": comic.id,
                        })
                        .context("failed to remove old comic_tags entries")?;
                }

                // Insert new comic_tags links
                {
                    let mut statement = transaction.prepare_cached(INSERT_COMIC_TAG)?;
                    for tag in comic_tags {
                        statement
                            .execute(named_params! {
                                ":comic_id": comic.id,
                                ":tag_id": tag.id,
                            })
                            .with_context(|| {
                                format!(
                                    "failed to insert comic_tag entry (comic_id={}, tag_id={})",
                                    comic.id, tag.id
                                )
                            })?;
                    }
                }

                transaction.commit()?;

                anyhow::Ok(())
            })
            .await
            .map_err(anyhow::Error::from)
            .and_then(std::convert::identity)
    }

    /// Get a comic
    #[allow(dead_code)]
    pub async fn get_comic(&self, id: NonZeroU64) -> anyhow::Result<Option<Comic>> {
        self.database
            .read(move |database| {
                database
                    .prepare_cached(GET_COMIC_SQL)?
                    .query_row(
                        named_params! {
                            ":id": id,
                        },
                        Comic::from_row,
                    )
                    .optional()
                    .context("failed to get comic")
            })
            .await
            .map_err(anyhow::Error::from)
            .and_then(std::convert::identity)
    }

    /// Get the images for a comic
    #[allow(dead_code)]
    pub async fn get_comic_images(&self, id: NonZeroU64) -> anyhow::Result<ComicImages> {
        self.database
            .read(move |database| {
                let mut cover = None;
                let mut pages = Vec::new();
                let mut thumbnail = None;

                let mut statement = database.prepare_cached(GET_COMIC_IMAGES_SQL)?;
                let rows = statement.query_map(
                    named_params! {
                        ":id": id,
                    },
                    |row| {
                        let image_type: ImageType = row.get("image_type")?;
                        let image = Image::from_row(row)?;
                        Ok((image_type, image))
                    },
                )?;
                for row in rows {
                    let (image_type, image) = row?;

                    match image_type {
                        ImageType::Cover => {
                            cover = Some(image);
                        }
                        ImageType::Page => {
                            pages.push(image);
                        }
                        ImageType::Thumbnail => {
                            thumbnail = Some(image);
                        }
                    }
                }

                Ok(ComicImages {
                    cover: cover.context("missing cover")?,
                    pages,
                    thumbnail: thumbnail.context("missing thumbnail")?,
                })
            })
            .await
            .map_err(anyhow::Error::from)
            .and_then(std::convert::identity)
    }

    /// Get the tags for a comic
    #[allow(dead_code)]
    pub async fn get_comic_tags(&self, id: NonZeroU64) -> anyhow::Result<Vec<Tag>> {
        self.database
            .read(move |database| {
                database
                    .prepare_cached(GET_COMIC_TAGS_SQL)?
                    .query_map(
                        named_params! {
                            ":id": id,
                        },
                        Tag::from_row,
                    )?
                    .collect::<Result<_, RusqliteError>>()
                    .context("failed to get comic tags")
            })
            .await
            .map_err(anyhow::Error::from)
            .and_then(std::convert::identity)
    }

    /// Delete a comic
    pub async fn delete_comic(&self, id: NonZeroU64) -> anyhow::Result<()> {
        self.database
            .write(move |database| {
                let transaction = database.transaction()?;
                {
                    let mut statement = transaction.prepare_cached(DELETE_COMIC_IMAGES_SQL)?;
                    statement.execute(named_params! {
                        ":id": id,
                    })?;
                }

                {
                    let mut statement = transaction.prepare_cached(DELETE_COMIC_TAGS)?;
                    statement.execute(named_params! {
                        ":id": id,
                    })?;
                }

                {
                    let mut statement = transaction.prepare_cached(DELETE_COMIC_SQL)?;
                    statement.execute(named_params! {
                        ":id": id,
                    })?;
                }

                Ok(())
            })
            .await
            .map_err(anyhow::Error::from)
            .and_then(std::convert::identity)
    }
}
