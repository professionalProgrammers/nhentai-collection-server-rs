use anyhow::bail;
use nd_async_rusqlite::rusqlite::types::FromSql;
use nd_async_rusqlite::rusqlite::types::FromSqlError;
use nd_async_rusqlite::rusqlite::types::ToSqlOutput;
use nd_async_rusqlite::rusqlite::types::ValueRef;
use nd_async_rusqlite::rusqlite::Result as RusqliteResult;
use nd_async_rusqlite::rusqlite::ToSql;

const JPG: &str = "jpg";
const PNG: &str = "png";
const GIF: &str = "gif";
const WEBP: &str = "webp";

/// The image format
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub enum ImageFormat {
    Jpg,
    Png,
    Gif,
    Webp,
}

impl ImageFormat {
    /// Get the 3-4 letter extension string of this image format
    pub fn as_str(self) -> &'static str {
        match self {
            Self::Jpg => JPG,
            Self::Png => PNG,
            Self::Gif => GIF,
            Self::Webp => WEBP,
        }
    }
}

impl std::str::FromStr for ImageFormat {
    type Err = anyhow::Error;

    fn from_str(text: &str) -> Result<Self, Self::Err> {
        match text {
            JPG => Ok(Self::Jpg),
            PNG => Ok(Self::Png),
            GIF => Ok(Self::Gif),
            WEBP => Ok(Self::Webp),
            _ => bail!("\"{text}\" is not a valid image format"),
        }
    }
}

impl ToSql for ImageFormat {
    fn to_sql(&self) -> RusqliteResult<ToSqlOutput<'_>> {
        self.as_str().to_sql()
    }
}

impl FromSql for ImageFormat {
    fn column_result(value: ValueRef<'_>) -> Result<Self, FromSqlError> {
        let text = value.as_str()?;
        text.parse::<Self>()
            .map_err(|error| FromSqlError::Other(error.into()))
    }
}

// TODO Re-export from crate top in upstream
impl From<nhentai::types::comic::ImageKind> for ImageFormat {
    fn from(kind: nhentai::ImageKind) -> Self {
        match kind {
            nhentai::ImageKind::Jpeg => Self::Jpg,
            nhentai::ImageKind::Png => Self::Png,
            nhentai::ImageKind::Gif => Self::Gif,
            nhentai::ImageKind::Webp => Self::Webp,
        }
    }
}
