use super::Image;

// TODO: Consider making an optional field on the comic type
/// Images in a comic
#[derive(Debug)]
pub struct ComicImages {
    /// The cover image
    pub cover: Image,

    /// The comic pages
    pub pages: Vec<Image>,

    /// The comic thumbnail
    pub thumbnail: Image,
}

impl From<&nhentai::types::comic::Images> for ComicImages {
    fn from(images: &nhentai::types::comic::Images) -> Self {
        Self {
            cover: Image::from(&images.cover),
            pages: images.pages.iter().map(Image::from).collect(),
            thumbnail: Image::from(&images.thumbnail),
        }
    }
}
