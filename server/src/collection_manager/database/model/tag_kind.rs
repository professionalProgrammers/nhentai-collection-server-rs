use anyhow::bail;
use nd_async_rusqlite::rusqlite::types::FromSql;
use nd_async_rusqlite::rusqlite::types::FromSqlError;
use nd_async_rusqlite::rusqlite::types::ToSqlOutput;
use nd_async_rusqlite::rusqlite::types::ValueRef;
use nd_async_rusqlite::rusqlite::Result as RusqliteResult;
use nd_async_rusqlite::rusqlite::ToSql;

const LANGUAGE: &str = "language";
const PARODY: &str = "parody";
const CHARACTER: &str = "character";
const TAG: &str = "tag";
const GROUP: &str = "group";
const ARTIST: &str = "artist";
const CATEGORY: &str = "category";

/// The kind of tag
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub enum TagKind {
    Language,
    Parody,
    Character,
    Tag,
    Group,
    Artist,
    Category,
}

impl TagKind {
    /// Get the &'static str label of this tag.
    pub fn as_str(self) -> &'static str {
        match self {
            Self::Language => LANGUAGE,
            Self::Parody => PARODY,
            Self::Character => CHARACTER,
            Self::Tag => TAG,
            Self::Group => GROUP,
            Self::Artist => ARTIST,
            Self::Category => CATEGORY,
        }
    }
}

impl std::str::FromStr for TagKind {
    type Err = anyhow::Error;

    fn from_str(text: &str) -> Result<Self, Self::Err> {
        match text {
            LANGUAGE => Ok(Self::Language),
            PARODY => Ok(Self::Parody),
            CHARACTER => Ok(Self::Character),
            TAG => Ok(Self::Tag),
            GROUP => Ok(Self::Group),
            ARTIST => Ok(Self::Artist),
            CATEGORY => Ok(Self::Category),
            _ => {
                bail!("\"{text}\" is not a valid tag kind")
            }
        }
    }
}

impl From<nhentai::TagKind> for TagKind {
    fn from(kind: nhentai::TagKind) -> Self {
        match kind {
            nhentai::TagKind::Language => Self::Language,
            nhentai::TagKind::Parody => Self::Parody,
            nhentai::TagKind::Character => Self::Character,
            nhentai::TagKind::Tag => Self::Tag,
            nhentai::TagKind::Group => Self::Group,
            nhentai::TagKind::Artist => Self::Artist,
            nhentai::TagKind::Category => Self::Category,
        }
    }
}

impl ToSql for TagKind {
    fn to_sql(&self) -> RusqliteResult<ToSqlOutput<'_>> {
        self.as_str().to_sql()
    }
}

impl FromSql for TagKind {
    fn column_result(value: ValueRef<'_>) -> RusqliteResult<Self, FromSqlError> {
        let text = value.as_str()?;
        text.parse::<Self>()
            .map_err(|error| FromSqlError::Other(error.into()))
    }
}
