use nd_async_rusqlite::rusqlite::types::FromSqlError;
use nd_async_rusqlite::rusqlite::Result as RusqliteResult;
use nd_async_rusqlite::rusqlite::Row as RusqliteRow;
use std::num::NonZeroU64;
use time::OffsetDateTime;

/// A comic record
#[derive(Debug)]
pub struct Comic {
    /// The comic id
    pub id: NonZeroU64,

    /// The pretty title
    pub pretty_title: Box<str>,

    /// The english title
    pub english_title: Box<str>,

    /// The japanese title
    pub japanese_title: Option<Box<str>>,

    /// The number of favorites
    pub num_favorites: u64,

    /// The upload date
    pub upload_date: OffsetDateTime,
}

impl Comic {
    /// Read this from a rusqlite row
    pub fn from_row(row: &RusqliteRow) -> RusqliteResult<Self> {
        let id = row.get("id")?;
        let pretty_title = row.get("pretty_title")?;
        let english_title = row.get("english_title")?;
        let japanese_title = row.get("japanese_title")?;
        let num_favorites = row.get("num_favorites")?;
        let upload_date: i64 = row.get("upload_date")?;

        let upload_date = OffsetDateTime::from_unix_timestamp(upload_date)
            .map_err(|error| FromSqlError::Other(error.into()))?;

        Ok(Self {
            id,
            pretty_title,
            english_title,
            japanese_title,
            num_favorites,
            upload_date,
        })
    }
}

impl From<&nhentai::Comic> for Comic {
    fn from(comic: &nhentai::Comic) -> Self {
        Self {
            id: comic.id,
            pretty_title: comic.title.pretty.clone(),
            english_title: comic.title.english.clone(),
            japanese_title: comic.title.japanese.clone(),
            num_favorites: comic.num_favorites,
            upload_date: comic.upload_date,
        }
    }
}
