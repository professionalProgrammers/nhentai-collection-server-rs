use super::ImageFormat;
use nd_async_rusqlite::rusqlite::Result as RusqliteResult;
use nd_async_rusqlite::rusqlite::Row as RusqliteRow;

/// An image
#[derive(Debug)]
pub struct Image {
    /// The image width
    pub width: u32,

    /// The image height
    pub height: u32,

    /// The image extension
    pub extension: ImageFormat,
}

impl From<&nhentai::types::comic::Image> for Image {
    fn from(image: &nhentai::types::comic::Image) -> Self {
        Self {
            width: image.width,
            height: image.height,
            extension: image.kind.into(),
        }
    }
}

impl Image {
    /// Read this from a rusqlite row
    pub fn from_row(row: &RusqliteRow) -> RusqliteResult<Self> {
        let width = row.get("width")?;
        let height = row.get("height")?;
        let extension = row.get("extension")?;

        Ok(Self {
            width,
            height,
            extension,
        })
    }
}
