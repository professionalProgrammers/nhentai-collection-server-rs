use super::TagKind;
use nd_async_rusqlite::rusqlite::Result as RusqliteResult;
use nd_async_rusqlite::rusqlite::Row as RusqliteRow;

/// A comic tag
#[derive(Debug)]
pub struct Tag {
    /// The id of the tag
    pub id: u64,

    /// The name of the tag
    pub name: Box<str>,

    /// The type of the tag
    pub kind: TagKind,
}

impl Tag {
    /// Create this from a rusqlite row
    pub fn from_row(row: &RusqliteRow) -> RusqliteResult<Self> {
        let id = row.get("id")?;
        let name = row.get("name")?;
        let kind = row.get("type")?;

        Ok(Self { id, name, kind })
    }
}

impl From<&nhentai::types::comic::Tag> for Tag {
    fn from(tag: &nhentai::types::comic::Tag) -> Self {
        Self {
            id: tag.id,
            name: tag.name.clone(),
            // TODO: Impl copy upstream.
            kind: tag.kind.clone().into(),
        }
    }
}
