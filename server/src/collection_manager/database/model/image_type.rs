use nd_async_rusqlite::rusqlite::types::FromSql;
use nd_async_rusqlite::rusqlite::types::FromSqlError;
use nd_async_rusqlite::rusqlite::types::ToSqlOutput;
use nd_async_rusqlite::rusqlite::types::ValueRef;
use nd_async_rusqlite::rusqlite::Result as RusqliteResult;
use nd_async_rusqlite::rusqlite::ToSql;

/// An error that occurs when an `ImageType` could not be parsed from a string.
#[derive(Debug)]
pub struct FromStrError(Box<str>);

impl std::fmt::Display for FromStrError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "\"{}\" is not a valid image type", self.0)
    }
}

impl std::error::Error for FromStrError {}

impl std::str::FromStr for ImageType {
    type Err = FromStrError;

    fn from_str(value: &str) -> Result<Self, Self::Err> {
        match value {
            "cover" => Ok(Self::Cover),
            "thumbnail" => Ok(Self::Thumbnail),
            "page" => Ok(Self::Page),
            value => Err(FromStrError(value.into())),
        }
    }
}

/// The type of an image
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum ImageType {
    /// A cover
    Cover,

    /// A thumbnail
    Thumbnail,

    /// A page
    Page,
}

impl ImageType {
    /// Get this as a static str.
    pub fn as_str(self) -> &'static str {
        match self {
            Self::Cover => "cover",
            Self::Thumbnail => "thumbnail",
            Self::Page => "page",
        }
    }
}

impl ToSql for ImageType {
    fn to_sql(&self) -> RusqliteResult<ToSqlOutput<'_>> {
        self.as_str().to_sql()
    }
}

impl FromSql for ImageType {
    fn column_result(value: ValueRef) -> RusqliteResult<Self, FromSqlError> {
        let value = value.as_str()?;

        value
            .parse::<Self>()
            .map_err(|e| FromSqlError::Other(e.into()))
    }
}
