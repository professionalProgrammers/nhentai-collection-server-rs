use std::num::NonZeroU64;

/// A preview of a Download
#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct DownloadPreview {
    /// The id of the download and comic
    pub id: NonZeroU64,
}
