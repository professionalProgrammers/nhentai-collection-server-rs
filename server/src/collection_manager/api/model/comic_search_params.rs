use std::collections::BTreeSet;

/// Parameters for searching for a comic
#[derive(Debug, PartialEq, Eq, Hash, serde::Deserialize)]
pub struct ComicSearchParams {
    /// The query
    pub query: Option<Box<str>>,

    /// The list of tags that comics must have to be returned.
    ///
    /// A set of empty tags indicates that all comics can be returned.
    #[serde(default, deserialize_with = "deserialize_tags")]
    pub tags: BTreeSet<Box<str>>,
}

fn deserialize_tags<'de, D>(deserializer: D) -> Result<BTreeSet<Box<str>>, D::Error>
where
    D: serde::Deserializer<'de>,
{
    use serde::Deserialize;

    let text = Box::<str>::deserialize(deserializer)?;
    Ok(text.split(',').map(Into::into).collect())
}
