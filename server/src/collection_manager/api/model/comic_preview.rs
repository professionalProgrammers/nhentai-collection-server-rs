use super::Image;
use std::num::NonZeroU64;

/// The preview for a comic
#[derive(Debug, serde::Serialize)]
pub struct ComicPreview {
    /// The ID of a comic
    pub id: NonZeroU64,

    /// The pretty title of a comic
    pub pretty_title: Box<str>,

    /// The english title of a comic
    pub english_title: Box<str>,

    /// The comic cover image
    pub cover_image: Image,
}
