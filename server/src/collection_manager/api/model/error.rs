/// An api error
#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct Error {
    /// Error message chain.
    pub messages: Vec<String>,
}

impl Error {
    /// Make an [`Error`] from an anyhow error.
    ///
    /// This is not provided as a from impl due to conflicting trait bounds.
    pub fn from_anyhow(error: &anyhow::Error) -> Self {
        Self {
            messages: error.chain().map(|error| error.to_string()).collect(),
        }
    }

    /// Make an [`Error`] from a message.
    pub fn from_message(message: impl Into<String>) -> Self {
        Self {
            messages: vec![message.into()],
        }
    }
}

impl<E> From<&E> for Error
where
    E: std::error::Error + 'static,
{
    fn from(error: &E) -> Self {
        Self {
            messages: anyhow::Chain::new(error)
                .map(|error| error.to_string())
                .collect(),
        }
    }
}
