use super::Image;
use super::Tag;

/// An API Comic
#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct Comic {
    /// Titles of the comic
    pub title: ComicTitle,

    /// Images
    pub images: ComicImages,

    /// Comic tags
    pub tags: Vec<Tag>,
}

/// Titles of a [`Comic`]
#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct ComicTitle {
    /// The "pretty" title
    pub pretty: Box<str>,
}

/// Images of a [`Comic`]
#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct ComicImages {
    /// The cover
    pub cover: Image,

    /// The comic pages
    pub pages: Vec<Image>,
}
