/// An Api Image
#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct Image {
    /// The width
    pub width: u32,

    /// The height
    pub height: u32,

    /// The extension
    pub extension: Box<str>,
}
