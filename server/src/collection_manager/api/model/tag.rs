/// A tag
#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct Tag {
    /// The tag id
    pub id: u64,

    /// The tag name
    pub name: Box<str>,

    /// The tag type
    pub kind: Box<str>,
}
