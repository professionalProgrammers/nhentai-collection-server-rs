pub(super) mod comic;
/// Database model types
pub mod model;

pub use self::model::Comic;
pub use self::model::ComicImages;
pub use self::model::Image;
pub use self::model::ImageFormat;
pub use self::model::ImageType;
pub use self::model::Tag;
pub use self::model::TagKind;
use anyhow::Context;
use camino::Utf8Path;
use nd_async_rusqlite::rusqlite::Connection as RusqliteConnection;
use nd_async_rusqlite::rusqlite::Result as RusqliteResult;
use std::num::NonZeroU64;
use std::time::Instant;
use tracing::error;
use tracing::info;

const SETUP_TABLES_SQL: &str =
    include_str!(concat!(env!("CARGO_MANIFEST_DIR"), "/sql/setup_tables.sql"));
const PURGE_SQL: &str = include_str!(concat!(env!("CARGO_MANIFEST_DIR"), "/sql/purge.sql"));
const OPTIMIZE_SQL: &str = include_str!(concat!(env!("CARGO_MANIFEST_DIR"), "/sql/optimize.sql"));

const GET_TAGS_SQL: &str = include_str!(concat!(env!("CARGO_MANIFEST_DIR"), "/sql/get_tags.sql"));

const GET_ALL_COMIC_IDS_SQL: &str = include_str!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/sql/get_all_comic_ids.sql"
));

const DATABASE_NAME: &str = "index.db";

const STATEMENT_CACHE_SIZE: usize = 128;

/// The database
#[derive(Debug, Clone)]
pub struct DatabaseTask {
    pub(crate) database: nd_async_rusqlite::WalPool,
}

impl DatabaseTask {
    /// Create a new [`Database`]
    pub async fn new<P>(path: P) -> anyhow::Result<Self>
    where
        P: AsRef<Utf8Path>,
    {
        let path = path.as_ref();
        let database_path = path.join_os(DATABASE_NAME);
        let database = nd_async_rusqlite::WalPool::builder()
            .writer_init_fn(|database| {
                database.set_prepared_statement_cache_capacity(STATEMENT_CACHE_SIZE);
                database.execute_batch(SETUP_TABLES_SQL)?;
                Ok(())
            })
            .reader_init_fn(|database| {
                database.set_prepared_statement_cache_capacity(STATEMENT_CACHE_SIZE);
                Ok(())
            })
            .num_read_connections(4)
            .open(&database_path)
            .await
            .context("failed to open sqlite index")?;

        Ok(Self { database })
    }

    /// Run a closure that has access to the entire database.
    pub async fn access_database<F, R>(&self, func: F) -> anyhow::Result<R>
    where
        F: FnOnce(&mut RusqliteConnection) -> R + Send + 'static,
        R: Send + 'static,
    {
        let start = Instant::now();
        let ret = self.database.write(func).await?;
        let elapsed = start.elapsed();
        info!("access_database executed in {elapsed:?}");
        Ok(ret)
    }

    /// Get all tags
    pub async fn get_tags(&self) -> anyhow::Result<Vec<Tag>> {
        self.database
            .read(|database| {
                database
                    .prepare_cached(GET_TAGS_SQL)?
                    .query_map([], Tag::from_row)?
                    .collect::<RusqliteResult<_>>()
                    .context("failed to get tags")
            })
            .await
            .map_err(anyhow::Error::from)
            .and_then(std::convert::identity)
    }

    /// Get all comic ids
    pub async fn get_all_comic_ids(&self) -> anyhow::Result<Vec<NonZeroU64>> {
        self.database
            .read(|database| {
                database
                    .prepare_cached(GET_ALL_COMIC_IDS_SQL)?
                    .query_map([], |row| row.get::<_, NonZeroU64>("id"))?
                    .collect::<RusqliteResult<Vec<_>>>()
                    .context("failed to get comic ids")
            })
            .await
            .map_err(anyhow::Error::from)
            .and_then(std::convert::identity)
    }

    /// Purge the index.
    ///
    /// Be VERY careful that you want to call this.
    /// This will NOT recreate tables and indexes.
    pub async fn purge(&self) -> anyhow::Result<()> {
        self.database
            .write(|database| {
                database
                    .execute_batch(PURGE_SQL)
                    .context("failed to purge database")
            })
            .await
            .map_err(anyhow::Error::from)
            .and_then(std::convert::identity)
    }

    /// Optimize the database.
    ///
    /// The database will become unresponsive while optimizing;
    /// make sure this is acceptable.
    pub async fn optimize(&self) -> anyhow::Result<()> {
        self.database
            .write(|database| {
                database
                    .execute_batch(OPTIMIZE_SQL)
                    .context("failed to optimize database")
            })
            .await
            .map_err(anyhow::Error::from)
            .and_then(std::convert::identity)
    }

    /// Shutdown the database.
    ///
    /// This will attempt to compress and optimize the database while exiting.
    pub async fn shutdown(&self) -> anyhow::Result<()> {
        // Optimize the database.
        let optimize_result = self.optimize().await.context("failed to optimize");
        if let Err(error) = optimize_result.as_ref() {
            error!("{error}");
        }

        // Ask the database to shutdown.
        let shutdown_result = self
            .database
            .close()
            .await
            .context("failed to shutdown database");
        if let Err(error) = shutdown_result.as_ref() {
            error!("{error}");
        }

        shutdown_result.and(optimize_result)
    }
}
