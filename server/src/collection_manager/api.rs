mod model;

pub use self::model::Comic;
pub use self::model::ComicImages;
pub use self::model::ComicPreview;
pub use self::model::ComicSearchParams;
pub use self::model::ComicTitle;
pub use self::model::DownloadPreview;
pub use self::model::Error;
pub use self::model::Image;
pub use self::model::Tag;
use super::database::comic::GET_COMIC_IMAGES_SQL;
use super::database::comic::GET_COMIC_SQL;
use super::database::comic::GET_COMIC_TAGS_SQL;
use super::DatabaseComic;
use super::DatabaseImage;
use super::DatabaseImageType;
use super::DatabaseTag;
use super::DatabaseTask;
use super::DownloadTask;
use anyhow::Context;
use bewu_util::AsyncTimedCacheCell;
use bewu_util::AsyncTimedLruCache;
use nd_async_rusqlite::rusqlite::named_params;
use nd_async_rusqlite::rusqlite::OptionalExtension;
use nd_async_rusqlite::rusqlite::Result as RusqliteResult;
use nd_util::ArcAnyhowError;
use std::sync::Arc;
use std::time::Duration;
use tokio::task::JoinSet;
use tracing::error;
use tracing::Instrument;

#[derive(Debug)]
enum Message {
    Close {
        tx: tokio::sync::oneshot::Sender<()>,
    },
    SearchComics {
        span: tracing::Span,
        tx: tokio::sync::oneshot::Sender<Result<Arc<[ComicPreview]>, ArcAnyhowError>>,
        params: ComicSearchParams,
    },
    GetComic {
        span: tracing::Span,
        tx: tokio::sync::oneshot::Sender<Result<Option<Arc<Comic>>, ArcAnyhowError>>,
        id: u64,
    },
    GetTags {
        span: tracing::Span,
        tx: tokio::sync::oneshot::Sender<Result<Arc<[Tag]>, ArcAnyhowError>>,
    },
    GetDownloads {
        span: tracing::Span,
        tx: tokio::sync::oneshot::Sender<Result<Arc<[DownloadPreview]>, ArcAnyhowError>>,
    },
}

/// Controls API accesses
#[derive(Debug, Clone)]
pub struct ApiTask {
    tx: tokio::sync::mpsc::Sender<Message>,
    handle: Arc<std::sync::Mutex<Option<tokio::task::JoinHandle<()>>>>,
}

impl ApiTask {
    /// Make a new indexer.
    pub fn new(database: DatabaseTask, download: DownloadTask) -> Self {
        let (tx, rx) = tokio::sync::mpsc::channel(1024);
        let handle = tokio::task::spawn(task_impl(
            rx,
            Arc::new(State {
                database,
                download,

                search_comics_cache: AsyncTimedLruCache::new(128, Duration::from_secs(0)),
                get_comic_cache: AsyncTimedLruCache::new(128, Duration::from_secs(0)),
                get_tags_cache: AsyncTimedLruCache::new(128, Duration::from_secs(0)),
                get_downloads_cache: AsyncTimedCacheCell::new(Duration::from_secs(0)),
            }),
        ));

        Self {
            tx,
            handle: Arc::new(std::sync::Mutex::new(Some(handle))),
        }
    }

    /// Search comics
    pub async fn search_comics(
        &self,
        params: ComicSearchParams,
    ) -> anyhow::Result<Arc<[ComicPreview]>> {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.tx
            .send(Message::SearchComics {
                span: tracing::Span::current(),
                tx,
                params,
            })
            .await?;
        Ok(rx.await??)
    }

    /// Get a comic by id.
    pub async fn get_comic(&self, id: u64) -> anyhow::Result<Option<Arc<Comic>>> {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.tx
            .send(Message::GetComic {
                span: tracing::Span::current(),
                tx,
                id,
            })
            .await?;
        Ok(rx.await??)
    }

    /// Get all tags
    pub async fn get_tags(&self) -> anyhow::Result<Arc<[Tag]>> {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.tx
            .send(Message::GetTags {
                span: tracing::Span::current(),
                tx,
            })
            .await?;
        Ok(rx.await??)
    }

    /// Get all downloads
    pub async fn get_downloads(&self) -> anyhow::Result<Arc<[DownloadPreview]>> {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.tx
            .send(Message::GetDownloads {
                span: tracing::Span::current(),
                tx,
            })
            .await?;
        Ok(rx.await??)
    }

    async fn close(&self) -> anyhow::Result<()> {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.tx.send(Message::Close { tx }).await?;
        rx.await.context("failed to get response from task")?;
        Ok(())
    }

    async fn join(&self) -> anyhow::Result<()> {
        let handle = self
            .handle
            .lock()
            .unwrap_or_else(|e| e.into_inner())
            .take()
            .context("missing handle")?;
        handle.await.context("failed to join handle")?;
        Ok(())
    }

    pub async fn shutdown(&self) -> anyhow::Result<()> {
        let close_result = self.close().await.context("failed to send close message");
        if let Err(error) = close_result.as_ref() {
            error!("{error}");
        }
        let join_result = self.join().await.context("failed to join");
        if let Err(error) = join_result.as_ref() {
            error!("{error}");
        }

        join_result.and(close_result)
    }
}

async fn task_impl(mut rx: tokio::sync::mpsc::Receiver<Message>, state: Arc<State>) {
    let mut task_set = JoinSet::<()>::new();

    loop {
        tokio::select! {
            Some(result) = task_set.join_next() => {
                if let Err(error) = result.context("failed to join task") {
                    error!("{error}");
                }
            }
            message = rx.recv() => {
                match message {
                    Some(Message::Close { tx }) => {
                        rx.close();

                        let _ = tx.send(()).is_ok();
                    }
                    Some(Message::SearchComics { span, tx, params }) => {
                        let state = state.clone();
                        task_set.spawn(async move {
                            let result = state
                                .search_comics(params)
                                .await;
                            let _ = tx.send(result).is_ok();
                        }.instrument(span));
                    }
                    Some(Message::GetComic { span, tx, id }) => {
                        let state = state.clone();
                        task_set.spawn(async move {
                            let result = state
                                .get_comic(id)
                                .await;
                            let _ = tx.send(result).is_ok();
                        }.instrument(span));
                    }
                    Some(Message::GetTags { span, tx }) => {
                        let state = state.clone();
                        task_set.spawn(async move {
                            let result = state
                                .get_tags()
                                .await;
                            let _ = tx.send(result).is_ok();
                        }.instrument(span));
                    }
                    Some(Message::GetDownloads { span, tx }) => {
                        let state = state.clone();
                        task_set.spawn(async move {
                            let result = state
                                .get_downloads()
                                .await;
                            let _ = tx.send(result).is_ok();
                        }.instrument(span));
                    }
                    None => {
                        break;
                    }
                }
            }
        }
    }
}

struct State {
    database: DatabaseTask,
    download: DownloadTask,

    search_comics_cache:
        AsyncTimedLruCache<Arc<ComicSearchParams>, Result<Arc<[ComicPreview]>, ArcAnyhowError>>,
    get_comic_cache: AsyncTimedLruCache<u64, Result<Option<Arc<Comic>>, ArcAnyhowError>>,
    get_tags_cache: AsyncTimedLruCache<(), Result<Arc<[Tag]>, ArcAnyhowError>>,
    get_downloads_cache: AsyncTimedCacheCell<Result<Arc<[DownloadPreview]>, ArcAnyhowError>>,
}

impl State {
    #[tracing::instrument(skip(self))]
    async fn search_comics(
        &self,
        mut params: ComicSearchParams,
    ) -> Result<Arc<[ComicPreview]>, ArcAnyhowError> {
        // Normalize query
        params.query = params.query.map(|query| query.to_ascii_lowercase().into());

        // Avoid cloning
        let params = Arc::new(params);

        self.search_comics_cache
            .get(params.clone(), || async {
                let sql = generate_search_comics_sql(&params);

                self.database
                    .access_database(move |database| {
                        let mut statement = database.prepare_cached(&sql)?;
                        if let Some(query) = params.query.as_ref() {
                            let index = statement
                                .parameter_index(":query")?
                                .context("missing \":query\" in sql statement")?;
                            statement.raw_bind_parameter(index, query)?;
                        }

                        for (i, tag) in params.tags.iter().enumerate() {
                            let param_name = format!(":tag_{i}");
                            let index =
                                statement.parameter_index(&param_name)?.with_context(|| {
                                    format!("missing \"{param_name}\" in sql statement")
                                })?;
                            statement.raw_bind_parameter(index, tag)?;
                        }

                        let result = statement
                            .raw_query()
                            .mapped(|row| {
                                let id = row.get("id")?;
                                let pretty_title = row.get("pretty_title")?;
                                let english_title = row.get("english_title")?;
                                let cover_image = DatabaseImage::from_row(row)?;

                                Ok(ComicPreview {
                                    id,
                                    pretty_title,
                                    english_title,
                                    cover_image: Image {
                                        width: cover_image.width,
                                        height: cover_image.height,
                                        extension: cover_image.extension.as_str().into(),
                                    },
                                })
                            })
                            .collect::<RusqliteResult<Arc<[ComicPreview]>>>()?;

                        anyhow::Ok(result)
                    })
                    .await
                    .context("failed to access database")
                    .and_then(std::convert::identity)
                    .map_err(ArcAnyhowError::new)
            })
            .await
    }

    #[tracing::instrument(skip(self))]
    async fn get_comic(&self, id: u64) -> Result<Option<Arc<Comic>>, ArcAnyhowError> {
        self.get_comic_cache
            .get(id, || async {
                self.database
                    .access_database(move |database| {
                        let transaction = database.transaction()?;

                        let database_comic = transaction
                            .prepare_cached(GET_COMIC_SQL)?
                            .query_row(
                                named_params! {
                                    ":id": id,
                                },
                                DatabaseComic::from_row,
                            )
                            .optional()
                            .context("failed to get comic")?;

                        let mut comic = match database_comic {
                            Some(comic) => Comic {
                                title: ComicTitle {
                                    pretty: comic.pretty_title,
                                },
                                images: ComicImages {
                                    cover: Image {
                                        width: 0,
                                        height: 0,
                                        extension: Default::default(),
                                    },
                                    pages: Vec::with_capacity(64),
                                },
                                tags: Vec::new(),
                            },
                            None => return Ok(None),
                        };

                        {
                            let mut statement = transaction.prepare_cached(GET_COMIC_IMAGES_SQL)?;
                            let rows = statement.query_map(
                                named_params! {
                                    ":id": id,
                                },
                                |row| {
                                    let image_type: DatabaseImageType = row.get(0)?;
                                    Ok((image_type, DatabaseImage::from_row(row)?))
                                },
                            )?;

                            for row in rows {
                                let (image_type, image) = row?;

                                match image_type {
                                    DatabaseImageType::Cover => {
                                        comic.images.cover = Image {
                                            width: image.width,
                                            height: image.height,
                                            extension: image.extension.as_str().into(),
                                        };
                                    }
                                    DatabaseImageType::Page => {
                                        comic.images.pages.push(Image {
                                            width: image.width,
                                            height: image.height,
                                            extension: image.extension.as_str().into(),
                                        });
                                    }
                                    DatabaseImageType::Thumbnail => {
                                        // TODO: Add to api type
                                    }
                                }
                            }
                        }

                        {
                            let tags = transaction
                                .prepare_cached(GET_COMIC_TAGS_SQL)?
                                .query_map(
                                    named_params! {
                                        ":id": id,
                                    },
                                    |row| {
                                        let tag = DatabaseTag::from_row(row)?;
                                        Ok(Tag {
                                            id: tag.id,
                                            name: tag.name,
                                            kind: tag.kind.as_str().into(),
                                        })
                                    },
                                )?
                                .collect::<RusqliteResult<Vec<_>>>()?;
                            comic.tags = tags;
                        }

                        anyhow::Ok(Some(Arc::new(comic)))
                    })
                    .await
                    .context("failed to access database")
                    .and_then(std::convert::identity)
                    .map_err(ArcAnyhowError::new)
            })
            .await
    }

    #[tracing::instrument(skip(self))]
    async fn get_tags(&self) -> Result<Arc<[Tag]>, ArcAnyhowError> {
        self.get_tags_cache
            .get((), || async {
                self.database
                    .get_tags()
                    .await
                    .map(|tags| {
                        tags.into_iter()
                            .map(|tag| Tag {
                                id: tag.id,
                                name: tag.name,
                                kind: tag.kind.as_str().into(),
                            })
                            .collect()
                    })
                    .context("failed to access database")
                    .map_err(ArcAnyhowError::new)
            })
            .await
    }

    #[tracing::instrument(skip(self))]
    async fn get_downloads(&self) -> Result<Arc<[DownloadPreview]>, ArcAnyhowError> {
        self.get_downloads_cache
            .get(|| async {
                self.download
                    .get_downloads()
                    .await
                    .map(|downloads| {
                        downloads
                            .into_iter()
                            .map(|download| DownloadPreview { id: download })
                            .collect()
                    })
                    .map_err(ArcAnyhowError::new)
            })
            .await
    }
}

fn generate_search_comics_sql(params: &ComicSearchParams) -> String {
    use std::fmt::Write;

    let where_query_fragment_sql = " 
        AND (
            INSTR(LOWER(comics.pretty_title), :query) > 0 OR 
            INSTR(LOWER(comics.english_title), :query) > 0 OR 
            INSTR(LOWER(CAST(comics.id AS TEXT)), :query) > 0 OR
            EXISTS (
                SELECT 
                    1
                FROM
                    comic_tags
                JOIN
                    tags
                ON
                    comic_tags.tag_id = tags.id
                WHERE 
                    comic_tags.comic_id = comics.id AND
                    INSTR(LOWER(tags.name), :query) > 0
            )
        )
        ";
    let where_query_fragment_sql = params
        .query
        .as_ref()
        .map(|_query| where_query_fragment_sql)
        .unwrap_or("");

    let mut where_tags_fragment_sql = String::with_capacity(256 * params.tags.len());
    for i in 0..params.tags.len() {
        write!(
            &mut where_tags_fragment_sql,
            "
            AND
                EXISTS (
                    SELECT
                        1
                    FROM
                        comic_tags
                    JOIN
                        tags
                    ON
                        comic_tags.tag_id = tags.id
                    WHERE
                        comics.id = comic_tags.comic_id AND
                        tags.name = :tag_{i}
                )
            "
        )
        .unwrap();
    }

    let sql = format!(
        "
        SELECT
            comics.id as id,
            comics.pretty_title as pretty_title,
            comics.english_title as english_title,
            images.width as width,
            images.height as height,
            images.extension as extension
        FROM
            comics
        JOIN
            images
        ON
            comics.id = images.comic_id
        WHERE
            images.image_type = \"cover\"
            {where_query_fragment_sql}
            {where_tags_fragment_sql}
        ORDER BY 
            comics.id;
        "
    );

    sql
}
