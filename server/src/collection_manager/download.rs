mod client;
mod download_state;

use self::client::Client;
pub use self::download_state::DownloadState;
pub use self::download_state::DownloadStateUpdate;
use super::DatabaseComic;
use super::DatabaseComicImages;
use super::DatabaseTag;
use super::DatabaseTask;
use crate::collection_manager::METADATA_FILE_NAME;
use crate::util::extract_url_file_name;
use crate::Config;
use anyhow::bail;
use anyhow::ensure;
use anyhow::Context;
use bewu_util::StateUpdateRx;
use bewu_util::StateUpdateTx;
use camino::Utf8Path;
use camino::Utf8PathBuf;
use nd_util::ArcAnyhowError;
use nd_util::DropRemovePath;
use reqwest::StatusCode;
use std::collections::btree_map::Entry as BTreeMapEntry;
use std::collections::BTreeMap;
use std::num::NonZeroU64;
use std::sync::Arc;
use task_system::Task;
use task_system::TaskContext;
use task_system::TaskHandle;
use tokio::io::AsyncWriteExt;
use tokio::task::JoinSet;
use tracing::error;
use tracing::warn;
use url::Url;

const STATE_UPDATE_CHANNEL_SIZE: usize = 512;

/// State needed for the download of an individual comic in the download map.
#[derive(Debug)]
struct DownloadMapState {
    /// The abort handle for the download task
    handle: tokio::task::AbortHandle,

    /// The state rx of the download
    state_rx: StateUpdateRx<DownloadState>,
}

/// A message for the download task
#[derive(Debug)]
enum Message {
    GetDownloads {
        tx: tokio::sync::oneshot::Sender<Vec<NonZeroU64>>,
    },
    CreateDownload {
        tx: tokio::sync::oneshot::Sender<bool>,
        id: NonZeroU64,
    },
    SubscribeDownload {
        tx: tokio::sync::oneshot::Sender<Option<StateUpdateRx<DownloadState>>>,
        id: NonZeroU64,
    },
    DeleteDownload {
        tx: tokio::sync::oneshot::Sender<anyhow::Result<bool>>,
        id: NonZeroU64,
    },
}

/// A task to manage downloads
#[derive(Debug, Clone)]
pub struct DownloadTask {
    handle: TaskHandle<Message>,
}

impl DownloadTask {
    /// Make a new [`DownloadTask`].
    pub fn new(config: Arc<Config>, database: DatabaseTask) -> anyhow::Result<Self> {
        let state = TaskState::new(config, database)?;
        let handle = task_system::spawn(state, 16);

        Ok(Self { handle })
    }

    /// Get downloads
    pub async fn get_downloads(&self) -> anyhow::Result<Vec<NonZeroU64>> {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.handle.send(Message::GetDownloads { tx }).await?;
        let response = rx.await.context("failed to get response from task")?;
        Ok(response)
    }

    /// Create a download.
    ///
    /// # Returns
    /// Returns true if a new download was created.
    /// Returns false if a download for this comic already exists.
    pub async fn create_download(&self, id: NonZeroU64) -> anyhow::Result<bool> {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.handle.send(Message::CreateDownload { tx, id }).await?;
        let response = rx.await.context("failed to get response from task")?;
        Ok(response)
    }

    /// Subscribe to the update stream for a download.
    ///
    /// # Returns
    /// Returns None if the download with the given id does not exist.
    pub async fn subscribe_download(
        &self,
        id: NonZeroU64,
    ) -> anyhow::Result<Option<StateUpdateRx<DownloadState>>> {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.handle
            .send(Message::SubscribeDownload { tx, id })
            .await?;
        let response = rx.await.context("failed to get response from task")?;
        Ok(response)
    }

    /// Delete a download.
    ///
    /// Currently, this will NOT work for on-going downloads.
    ///
    /// # Returns
    /// Returns true if the download was deleted.
    /// Returns false if the download did not exist.
    pub async fn delete_download(&self, id: NonZeroU64) -> anyhow::Result<bool> {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.handle.send(Message::DeleteDownload { tx, id }).await?;
        let response = rx.await.context("failed to get response from task")??;
        Ok(response)
    }

    /// Join and shutdown the task
    pub async fn shutdown(&self) -> anyhow::Result<()> {
        self.handle
            .close_and_join()
            .await
            .map_err(anyhow::Error::from)
    }
}

struct TaskState {
    downloads: BTreeMap<NonZeroU64, DownloadMapState>,

    inner: Arc<InnerTaskState>,
}

impl TaskState {
    fn new(config: Arc<Config>, database: DatabaseTask) -> anyhow::Result<Self> {
        Ok(Self {
            downloads: BTreeMap::new(),

            inner: Arc::new(InnerTaskState {
                database,

                base_path: config.collection_directory.clone(),
                client: Client::new(config)?,
            }),
        })
    }

    /// Get the list of downloads
    ///
    /// # Returns
    /// Returns the list of download ids, from least to greatest.
    fn get_downloads(&self) -> Vec<NonZeroU64> {
        self.downloads.keys().copied().collect()
    }

    fn create_download(&mut self, id: NonZeroU64, context: &mut TaskContext<Message>) -> bool {
        let entry = self.downloads.entry(id);

        let entry = match entry {
            BTreeMapEntry::Occupied(_entry) => return false,
            BTreeMapEntry::Vacant(entry) => entry,
        };

        let state = DownloadState::new();
        let (state_tx, state_rx) =
            bewu_util::state_update_channel(STATE_UPDATE_CHANNEL_SIZE, state);
        let handle = context.spawn(perform_download(self.inner.clone(), id, state_tx));

        entry.insert(DownloadMapState { handle, state_rx });

        true
    }

    fn delete_download(&mut self, id: NonZeroU64) -> anyhow::Result<bool> {
        let entry = self.downloads.entry(id);

        let entry = match entry {
            BTreeMapEntry::Occupied(entry) => entry,
            BTreeMapEntry::Vacant(_entry) => return Ok(false),
        };

        // TODO: Until we make download tasks abort-safe,
        // disallow interruping an ongoing download.
        ensure!(
            entry.get().handle.is_finished(),
            "the download is still in progress"
        );

        entry.remove();

        Ok(true)
    }
}

impl Task for TaskState {
    type Message = Message;

    fn process_message(&mut self, message: Message, context: &mut TaskContext<Message>) {
        match message {
            Message::GetDownloads { tx } => {
                let downloads = self.get_downloads();
                let _ = tx.send(downloads).is_ok();
            }
            Message::CreateDownload { id, tx } => {
                let created = self.create_download(id, context);
                let _ = tx.send(created).is_ok();
            }
            Message::SubscribeDownload { id, tx } => {
                let state_rx = self.downloads.get(&id).map(|state| state.state_rx.clone());
                let _ = tx.send(state_rx).is_ok();
            }
            Message::DeleteDownload { id, tx } => {
                let result = self.delete_download(id);
                let _ = tx.send(result).is_ok();
            }
        }
    }

    fn process_join_result(
        &mut self,
        result: Result<(), tokio::task::JoinError>,
        _context: &mut TaskContext<Message>,
    ) {
        if let Err(error) = result {
            error!("{error}");
        }
    }
}

struct InnerTaskState {
    database: DatabaseTask,

    base_path: Utf8PathBuf,
    client: Client,
}

impl InnerTaskState {
    /// Fetch metadata, using nhentai.to as a backup.
    ///
    /// # Returns
    /// Returns a tuple.
    /// The first element is the result of the fetch operation.
    /// The second element is a boolean,
    /// The second element is true if the result corresponds to nhentai.net.
    /// The second element is false if the result corresponds with nhentai.to.
    async fn get_metadata(
        &self,
        id: NonZeroU64,
    ) -> (Result<Arc<nhentai::Comic>, ArcAnyhowError>, bool) {
        let comic_result = self
            .client
            .get_comic(id)
            .await
            .map(Arc::new)
            .map_err(ArcAnyhowError::new);

        let comic_error = match comic_result {
            Ok(comic) => {
                return (Ok(comic), true);
            }
            Err(error) => error,
        };

        // Hack around deleted entries by hoping they got peristed to nhentai.to.
        let comic_deleted = {
            let reqwest_error = anyhow::Chain::new(&comic_error)
                .last()
                .and_then(|error| error.downcast_ref::<reqwest::Error>());
            let status_code = reqwest_error.and_then(|error| error.status());

            status_code == Some(reqwest::StatusCode::NOT_FOUND)
        };

        // Hack around nhentai telling us to fuck off by using nhentai.to.
        let nhentai_block = {
            let reqwest_error = anyhow::Chain::new(&comic_error)
                .last()
                .and_then(|error| error.downcast_ref::<reqwest::Error>());
            let status_code = reqwest_error.and_then(|error| error.status());

            status_code == Some(reqwest::StatusCode::FORBIDDEN)
        };

        if comic_deleted {
            warn!("comic {id} was potentially deleted, attempting to use nhentai.to");
        }

        if nhentai_block {
            warn!("nhentai.net blocked us, attempting to use nhentai.to");
        }

        if !comic_deleted && !nhentai_block {
            return (Err(comic_error), true);
        }

        let result = async {
            let comic = self.client.get_comic_nhentai_to(id).await?;
            let comic = nhentai_to_comic2nhentai_comic(comic)?;

            anyhow::Ok(comic)
        }
        .await
        .context("failed to get metadata from nhentai.to")
        .map(Arc::new)
        .map_err(ArcAnyhowError::new);

        (result, false)
    }
}

async fn perform_download(
    state: Arc<InnerTaskState>,
    id: NonZeroU64,
    state_tx: StateUpdateTx<DownloadState>,
) {
    let base_path = state.base_path.join(itoa::Buffer::new().format(id.get()));

    let (comic_result, is_nhentai) = state.get_metadata(id).await;

    // Send metadata downloaded notification.
    state_tx.send(DownloadStateUpdate::MetadataDownloaded {
        result: comic_result.clone(),
    });

    let comic = match comic_result {
        Ok(comic) => comic,
        Err(error) => {
            error!("{error:?}");
            return;
        }
    };

    let mut image_base_url = "https://i1.nhentai.net/galleries";
    let mut thumb_base_url = "https://t1.nhentai.net/galleries";
    let image_base_invalid = if is_nhentai {
        false
    } else {
        let url = comic.iter_image_urls_with_base_url(image_base_url).next();
        match url {
            Some(url) => {
                let result = state
                    .client
                    .inner
                    .client
                    .client
                    .get(url)
                    .send()
                    .await
                    .context("failed to check if image exists");
                match result {
                    Ok(response) => response.status() == StatusCode::NOT_FOUND,
                    Err(error) => {
                        warn!("{error:?}");

                        // TODO: Consider adding error type instead of silent fallback.
                        false
                    }
                }
            }
            None => false,
        }
    };

    if image_base_invalid {
        image_base_url = "https://cdn.dogehls.xyz/galleries";
        thumb_base_url = "https://cdn.dogehls.xyz/galleries";
    }

    let download_list = generate_download_list(&comic, image_base_url, thumb_base_url, &state_tx);

    // Notify that the download list was generated
    state_tx.send(DownloadStateUpdate::DownloadListGenerated {
        download_list: download_list.clone(),
    });

    let mut join_set = JoinSet::new();

    // TODO:
    // Maybe there should be a global type that controls comic fs access.
    // Like a `ComicFsManager` type that is a HashMap of u64 (id) => `ComicLock`
    // Which can do certain actions on drop, on top of "unlocking" the dir like removing a dir or creating a file.
    //
    // We also probably? need to use a temp dir here and rename to the actual dir.
    // Create out dir
    let result = tokio::fs::create_dir_all(&base_path)
        .await
        .context("failed to create dir")
        .map_err(ArcAnyhowError::new);

    if let Err(error) = result {
        // Alert that we failed to create the comic dir.
        //
        // This is fatal.
        state_tx.send(DownloadStateUpdate::CreateComicDirFail { error });
        return;
    }

    // Save metadata
    {
        let base_path = base_path.clone();
        let state = state.clone();
        let state_tx = state_tx.clone();

        join_set.spawn(async move {
            let result = async {
                {
                    let comic_tags = comic.tags.iter().map(DatabaseTag::from).collect();
                    let comic_images = DatabaseComicImages::from(&comic.images);
                    let comic = DatabaseComic::from(&*comic);

                    state
                        .database
                        .upsert_comic(comic, comic_images, comic_tags)
                        .await
                        .with_context(|| format!("failed to upsert comic {id}"))?;
                }

                save_metadata_to_path(base_path, &comic).await
            }
            .await
            .map_err(ArcAnyhowError::new);

            // Notify that a file was saved
            state_tx.send(DownloadStateUpdate::FileSaved {
                file_name: METADATA_FILE_NAME.into(),
                result,
            });
        });
    }

    // Perform download
    for (file_name, url) in download_list
        .iter()
        .filter_map(|(filename, url)| url.clone().map(|url| (filename.clone(), url)))
    {
        let state_tx = state_tx.clone();
        let client = state.client.clone();
        let path = base_path.join(&*file_name);

        join_set.spawn(async move {
            let result = async {
                // First, check if the file exists.
                // This is safe as we have exclusive access to the file.
                if tokio::fs::try_exists(&path)
                    .await
                    .context("failed to get file metadata")?
                {
                    return Ok(false);
                }

                let _permit = client
                    .inner
                    .image_semaphore
                    .acquire()
                    .await
                    .context("image semaphore closed")?;

                let reqwest_client = if image_base_invalid {
                    &client.inner.nhentai_to_client.client
                } else {
                    &client.inner.client.client
                };

                nd_util::download_to_path(reqwest_client, url.as_str(), path)
                    .await
                    .context("failed to download to file")?;

                Ok(true)
            }
            .await
            .map_err(|error| {
                error!("{error:?}");
                ArcAnyhowError::new(error)
            });

            // Notify that a file was saved
            state_tx.send(DownloadStateUpdate::FileSaved { file_name, result });
        });
    }

    // Wait for all downloads to finish, log panics if they occur
    while let Some(result) = join_set.join_next().await {
        // TODO: Consider reporting errors to stream.
        match result.context("a task failed to join") {
            Ok(()) => {}
            Err(error) => {
                error!("{error:?}");
            }
        }
    }

    // TODO:
    // Persist data?
    // The only data that possibly needs to be persisted is what is being downlaoded.
    // This is beacuse we need to load incomplete donloads on startup and download the missing assets.
    // The verifier task can also do this.
    // However, its more direct if we load with the verifier task, and we need to keep track of what assets havent been downloaded to prevent access to them.
}

/// Save metadata to a path.
async fn save_metadata_to_path<P>(base_path: P, comic: &nhentai::Comic) -> anyhow::Result<bool>
where
    P: AsRef<Utf8Path>,
{
    let path = base_path.as_ref().join(METADATA_FILE_NAME);

    // No need for checking if the file exists.
    // We always want to get the latest metadata,
    // and we know for a fact we have exclusive access to this directory
    // since this is used as a part of the downlaod task,
    // which "owns" the comic directory.

    let tmp_path = nd_util::with_push_extension(&path, "part");
    let mut file = tokio::fs::File::create(&tmp_path)
        .await
        .context("failed to create metadata file")?;
    let mut tmp_path = DropRemovePath::new(tmp_path);
    let data = serde_json::to_vec_pretty(comic).context("failed to serialize metadata")?;
    file.write_all(&data)
        .await
        .context("failed to write metadata")?;
    file.flush().await.context("failed to flush file")?;
    file.sync_all().await.context("failed to sync file")?;

    tokio::fs::rename(&tmp_path, &path)
        .await
        .context("failed to rename tmp file")?;
    tmp_path.persist();

    Ok(true)
}

fn generate_download_list(
    comic: &nhentai::Comic,
    image_base_url: &str,
    thumb_base_url: &str,
    state_tx: &StateUpdateTx<DownloadState>,
) -> Arc<[(Box<str>, Option<Url>)]> {
    // Reserve # of pages + thumbnail + cover + metadata
    let mut download_list: Vec<(Box<str>, Option<Url>)> =
        Vec::with_capacity(comic.images.pages.len() + 1 + 1 + 1);

    // Add metadata file, even though it is technically already "downloaded".
    // This will let the user know the progress/result of saving it.
    download_list.push((METADATA_FILE_NAME.into(), None));

    for url in comic
        .iter_image_urls_with_base_url(image_base_url)
        .chain(std::iter::once(
            comic.cover_image_url_with_base_url(thumb_base_url),
        ))
        .chain(std::iter::once(
            comic.thumbnail_image_url_with_base_url(thumb_base_url),
        ))
    {
        match extract_url_file_name(&url) {
            Some(file_name) => {
                download_list.push((file_name.into(), Some(url)));
            }
            None => {
                // Notify that an entry could not be added to the download list
                state_tx.send(DownloadStateUpdate::InvalidDownloadListEntry { url });
            }
        }
    }

    Arc::from(download_list)
}

fn nhentai_to_comic2nhentai_comic(comic: nhentai_to::Comic) -> anyhow::Result<nhentai::Comic> {
    let title = nhentai::types::comic::Title {
        english: html_unescape(&comic.title.english).into(),
        japanese: comic.title.japanese,
        pretty: html_unescape(&comic.title.pretty).into(),
        unknown: comic.title.unknown,
    };
    let images_pages = match comic.images.pages {
        serde_json::Value::Array(array) => {
            let mut images_pages = Vec::with_capacity(array.len());
            for image in array {
                let image: nhentai::types::comic::Image = serde_json::from_value(image)?;
                images_pages.push(image);
            }

            ensure!(images_pages.len() == usize::try_from(comic.num_pages)?);

            images_pages
        }
        _ => bail!("unknown Images.pages field"),
    };
    let images = nhentai::types::comic::Images {
        pages: images_pages,
        cover: nhentai_to_image2nhentai_image(comic.images.cover)?,
        thumbnail: nhentai_to_image2nhentai_image(comic.images.thumbnail)?,
        unknown: comic.images.unknown,
    };
    Ok(nhentai::Comic {
        id: comic.id,
        media_id: comic.media_id.parse()?,
        title,
        images,
        scanlator: comic.scanlator,
        upload_date: comic.upload_date,
        tags: comic
            .tags
            .into_iter()
            .map(|tag| {
                Ok(nhentai::types::comic::Tag {
                    id: tag.nh_id.parse()?,
                    kind: match tag.kind {
                        nhentai_to::TagKind::Language => nhentai::TagKind::Language,
                        nhentai_to::TagKind::Parody => nhentai::TagKind::Parody,
                        nhentai_to::TagKind::Tag => nhentai::TagKind::Tag,
                        nhentai_to::TagKind::Group => nhentai::TagKind::Group,
                        nhentai_to::TagKind::Artist => nhentai::TagKind::Artist,
                        nhentai_to::TagKind::Character => nhentai::TagKind::Character,
                        nhentai_to::TagKind::Category => nhentai::TagKind::Category,
                    },
                    name: tag.name,
                    url: tag.url,
                    // We do not get this field, we cannot fill it in.
                    // Not too important, if we are using this object the API has already nuked the metadata for this;
                    // count does not exist for this anymore.
                    count: 0,

                    unknown: tag.unknown,
                })
            })
            .collect::<anyhow::Result<_>>()?,
        num_pages: comic.num_pages,
        // We do not get this field, we cannot fill it in.
        // Not too important, if we are using this object the API has already nuked the metadata for this;
        // favorites does not exist for this anymore.
        num_favorites: 0,
        unknown: comic.unknown,
    })
}

fn nhentai_to_image2nhentai_image(image: nhentai_to::Image) -> anyhow::Result<nhentai::Image> {
    Ok(nhentai::Image {
        width: image.width,
        height: image.height,
        kind: match image.kind {
            nhentai_to::ImageKind::Jpeg => nhentai::ImageKind::Jpeg,
            nhentai_to::ImageKind::Png => nhentai::ImageKind::Png,
            nhentai_to::ImageKind::Gif => nhentai::ImageKind::Gif,
            nhentai_to::ImageKind::Webp => nhentai::ImageKind::Webp,
        },
        unknown: image.unknown,
    })
}

fn html_unescape(input: &str) -> String {
    input.replace("&#039;", "'")
}
