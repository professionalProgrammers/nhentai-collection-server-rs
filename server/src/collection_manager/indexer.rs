use super::DatabaseComic;
use super::DatabaseComicImages;
use super::DatabaseTag;
use super::DatabaseTask;
use super::METADATA_FILE_NAME;
use anyhow::anyhow;
use anyhow::Context;
use bewu_util::AsyncTimedLruCache;
use camino::Utf8PathBuf;
use nd_util::ArcAnyhowError;
use std::collections::HashSet;
use std::num::NonZeroU64;
use std::sync::Arc;
use std::time::Duration;
use std::time::Instant;
use task_system::Task;
use task_system::TaskContext;
use task_system::TaskHandle;
use tokio::task::JoinSet;
use tracing::error;
use tracing::info;
use tracing::Instrument;

#[derive(Debug)]
enum Message {
    Validate {
        tx: tokio::sync::oneshot::Sender<Result<bool, ArcAnyhowError>>,
        span: tracing::Span,
        id: NonZeroU64,
    },
    ValidateAll {
        tx: tokio::sync::oneshot::Sender<Result<(), anyhow::Error>>,
        span: tracing::Span,
    },
}

/// An indexer for the collection
#[derive(Debug, Clone)]
pub struct IndexerTask {
    handle: TaskHandle<Message>,
}

impl IndexerTask {
    /// Make a new indexer.
    pub fn new(database: DatabaseTask, base_path: Utf8PathBuf) -> Self {
        let handle = task_system::spawn(TaskState::new(database, base_path), 1024);
        Self { handle }
    }

    /// Validate a comic.
    ///
    /// # Returns
    /// Returns true if the comic was validated.
    /// Returns false if the comic was deleted.
    #[allow(dead_code)] // We may expose to api in the future
    pub async fn validate(&self, id: NonZeroU64) -> anyhow::Result<bool> {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.handle
            .send(Message::Validate {
                tx,
                span: tracing::Span::current(),
                id,
            })
            .await?;
        Ok(rx.await??)
    }

    /// Validate all comics
    pub async fn validate_all(&self) -> anyhow::Result<()> {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.handle
            .send(Message::ValidateAll {
                tx,
                span: tracing::Span::current(),
            })
            .await?;
        rx.await?
    }

    /// Shutdown this indexer.
    ///
    /// This may only be called once per object
    pub async fn shutdown(&self) -> anyhow::Result<()> {
        self.handle
            .close_and_join()
            .await
            .map_err(anyhow::Error::from)
    }
}

#[derive(Debug)]
struct TaskState {
    inner: Arc<InnerTaskState>,

    validate_all_handle: Option<tokio::task::AbortHandle>,
}

impl TaskState {
    fn new(database: DatabaseTask, base_path: Utf8PathBuf) -> Self {
        Self {
            inner: Arc::new(InnerTaskState {
                database,

                base_path,

                validate_cache: AsyncTimedLruCache::new(1024, Duration::from_secs(0)),
            }),

            validate_all_handle: None,
        }
    }
}

impl Task for TaskState {
    type Message = Message;

    fn process_message(&mut self, message: Message, context: &mut TaskContext<Message>) {
        match message {
            Message::Validate { tx, span, id } => {
                let state = self.inner.clone();
                context.spawn(
                    async move {
                        let result = state.validate(id).await;
                        let _ = tx.send(result).is_ok();
                    }
                    .instrument(span),
                );
            }
            Message::ValidateAll { tx, span } => match self.validate_all_handle.as_ref() {
                Some(handle) if !handle.is_finished() => {
                    let _ = tx
                        .send(Err(anyhow!("another operation is already in progress")))
                        .is_ok();
                }
                Some(_) | None => {
                    let state = self.inner.clone();
                    context.spawn(
                        async move {
                            let result = state.validate_all().await;
                            let _ = tx.send(result).is_ok();
                        }
                        .instrument(span),
                    );
                }
            },
        }
    }

    fn process_join_result(
        &mut self,
        result: Result<(), tokio::task::JoinError>,
        _context: &mut TaskContext<Message>,
    ) {
        if let Err(error) = result {
            error!("{error}");
        }
    }
}

#[derive(Debug)]
struct InnerTaskState {
    database: DatabaseTask,

    base_path: Utf8PathBuf,

    validate_cache: AsyncTimedLruCache<NonZeroU64, Result<bool, ArcAnyhowError>>,
}

impl InnerTaskState {
    #[tracing::instrument(skip(self))]
    async fn validate(&self, id: NonZeroU64) -> Result<bool, ArcAnyhowError> {
        self.validate_cache
            .get(id, || async {
                async {
                    let comic_path = self.base_path.join(itoa::Buffer::new().format(id.get()));
                    let metadata_path = comic_path.join(METADATA_FILE_NAME);

                    let raw_json = match tokio::fs::read_to_string(metadata_path).await {
                        Ok(raw_json) => raw_json,
                        Err(e) if e.kind() == std::io::ErrorKind::NotFound => {
                            // The data has been deleted from the disk, remove from database as well.
                            self.database.delete_comic(id).await?;

                            return Ok(false);
                        }
                        Err(e) => {
                            return Err(e).context("failed to read metadata");
                        }
                    };

                    let comic: nhentai::Comic =
                        serde_json::from_str(&raw_json).context("failed to parse metadata")?;
                    let comic_tags = comic.tags.iter().map(DatabaseTag::from).collect();
                    let comic_images = DatabaseComicImages::from(&comic.images);
                    let comic = DatabaseComic::from(&comic);

                    self.database
                        .upsert_comic(comic, comic_images, comic_tags)
                        .await
                        .with_context(|| format!("failed to upsert comic \"{id}\""))?;

                    Ok(true)
                }
                .await
                .map_err(ArcAnyhowError::new)
            })
            .await
    }

    async fn validate_all(self: Arc<Self>) -> anyhow::Result<()> {
        let start = Instant::now();

        let database_comic_ids = {
            let state = self.clone();
            tokio::spawn(async move { state.database.get_all_comic_ids().await })
        };

        let mut file_comic_ids = {
            let state = self.clone();
            tokio::task::spawn_blocking(move || {
                let mut file_comic_ids = HashSet::new();
                let dir_iter = state.base_path.read_dir_utf8()?;
                for entry in dir_iter {
                    let entry = entry?;
                    let maybe_id: Option<NonZeroU64> = entry.file_name().parse().ok();
                    if let Some(id) = maybe_id {
                        file_comic_ids.insert(id);
                    }
                }

                anyhow::Ok(file_comic_ids)
            })
            .await??
        };

        let database_comic_ids = database_comic_ids.await??;

        for id in database_comic_ids {
            if !file_comic_ids.remove(&id) {
                // There exists data in the index which is not on disk.
                // We need to re-index this data to remove it from the database.
                file_comic_ids.insert(id);
            }
        }

        info!("indexing {} entries after pruning", file_comic_ids.len());

        let mut join_set = JoinSet::new();
        for id in file_comic_ids.into_iter() {
            let state = self.clone();
            join_set.spawn(async move { state.validate(id).await });
        }

        let mut last_error = Ok(());
        while let Some(result) = join_set.join_next().await {
            let result = result
                .context("failed to join task")
                .and_then(|result| result.context("failed to index comic"))
                .map(|_| ());
            if let Err(error) = result {
                error!("{error:?}");

                last_error = Err(error);
            }
        }

        last_error?;

        let elapsed = start.elapsed();
        info!("index_all executed in {elapsed:?}");
        Ok(())
    }
}
