use crate::collection_manager::ApiError;
use crate::CollectionManager;
use axum::extract::State;
use axum::http::StatusCode;
use axum::response::IntoResponse;
use axum::routing::get;
use axum::Json;
use axum::Router;
use std::sync::Arc;
use tracing::error;

pub fn routes() -> Router<Arc<CollectionManager>> {
    Router::new()
        .route("/optimize", get(get_optimize))
        .route("/purge", get(get_purge))
}

async fn get_optimize(
    State(collection_manager): State<Arc<CollectionManager>>,
) -> impl IntoResponse {
    collection_manager
        .database
        .optimize()
        .await
        .as_ref()
        .map(|()| (StatusCode::OK, Json("ok")).into_response())
        .map_err(|error| {
            error!("{error}");

            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(ApiError::from_anyhow(error)),
            )
        })
}

async fn get_purge(State(collection_manager): State<Arc<CollectionManager>>) -> impl IntoResponse {
    collection_manager
        .database
        .purge()
        .await
        .as_ref()
        .map(|()| (StatusCode::OK, Json("ok, ur the boss")))
        .map_err(|error| {
            error!("{error}");

            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(ApiError::from_anyhow(error)),
            )
        })
}
