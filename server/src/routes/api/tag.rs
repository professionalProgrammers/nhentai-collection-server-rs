use super::CollectionManager;
use crate::collection_manager::ApiError;
use axum::extract::State;
use axum::http::StatusCode;
use axum::response::IntoResponse;
use axum::routing::get;
use axum::Json;
use axum::Router;
use std::sync::Arc;
use tracing::error;

pub fn routes() -> Router<Arc<CollectionManager>> {
    Router::new().route("/", get(list_tags))
}

async fn list_tags(State(collection_manager): State<Arc<CollectionManager>>) -> impl IntoResponse {
    collection_manager
        .api
        .get_tags()
        .await
        .as_deref()
        .map(|list| (StatusCode::OK, Json(list)).into_response())
        .map_err(|error| {
            error!("{error}");

            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(ApiError::from_anyhow(error)),
            )
        })
}
