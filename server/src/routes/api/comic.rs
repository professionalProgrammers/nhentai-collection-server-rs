use crate::collection_manager::ApiComicSearchParams;
use crate::collection_manager::ApiError;
use crate::CollectionManager;
use axum::extract::Path;
use axum::extract::Query;
use axum::extract::State;
use axum::http::StatusCode;
use axum::response::IntoResponse;
use axum::routing::get;
use axum::Json;
use axum::Router;
use std::sync::Arc;
use tracing::error;

pub fn routes() -> Router<Arc<CollectionManager>> {
    Router::new()
        .route("/", get(get_index))
        .route("/:comic_id", get(get_comic_id))
}

async fn get_index(
    State(collection_manager): State<Arc<CollectionManager>>,
    Query(params): Query<ApiComicSearchParams>,
) -> impl IntoResponse {
    collection_manager
        .api
        .search_comics(params)
        .await
        .as_deref()
        .map(|list| (StatusCode::OK, Json(list)).into_response())
        .map_err(|error| {
            error!("{error}");

            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(ApiError::from_anyhow(error)),
            )
        })
}

async fn get_comic_id(
    State(collection_manager): State<Arc<CollectionManager>>,
    Path(comic_id): Path<u64>,
) -> impl IntoResponse {
    collection_manager
        .api
        .get_comic(comic_id)
        .await
        .as_ref()
        .map(|comic| match comic.as_deref() {
            Some(comic) => (StatusCode::OK, Json(comic)).into_response(),
            None => (
                StatusCode::NOT_FOUND,
                Json(ApiError::from_message("comic not found")),
            )
                .into_response(),
        })
        .map_err(|error| {
            error!("{error}");

            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(ApiError::from_anyhow(error)),
            )
        })
}
