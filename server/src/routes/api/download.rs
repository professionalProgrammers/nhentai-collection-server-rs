use crate::routes::ApiError;
use crate::routes::ApiHandler;
use crate::CollectionManager;
use axum::extract::Path;
use axum::extract::State;
use axum::http::StatusCode;
use axum::response::sse;
use axum::response::IntoResponse;
use axum::response::Sse;
use axum::routing::get;
use axum::Extension;
use axum::Json;
use axum::Router;
use std::num::NonZeroU64;
use std::sync::Arc;
use tokio_stream::StreamExt;
use tracing::error;

pub fn routes() -> Router<Arc<CollectionManager>> {
    Router::new()
        .route("/", get(get_index))
        .route("/:id", get(get_download).post(post_id).delete(delete_id))
}

async fn get_index(State(collection_manager): State<Arc<CollectionManager>>) -> impl IntoResponse {
    collection_manager
        .api
        .get_downloads()
        .await
        .as_deref()
        .map(|downloads| (StatusCode::OK, Json(downloads)).into_response())
        .map_err(|error| {
            error!("{error}");

            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(ApiError::from_anyhow(error)),
            )
        })
}

async fn post_id(
    State(collection_manager): State<Arc<CollectionManager>>,
    Path(id): Path<NonZeroU64>,
) -> impl IntoResponse {
    collection_manager
        .download
        .create_download(id)
        .await
        .as_ref()
        .map(|created| match created {
            true => (StatusCode::OK, Json("ok")).into_response(),
            false => (
                StatusCode::CONFLICT,
                Json("a download with the given id already exists"),
            )
                .into_response(),
        })
        .map_err(|error| {
            error!("{error}");

            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(ApiError::from_anyhow(error)),
            )
        })
}

/// Path handler for `get("/download/{comic_id}")`
async fn get_download(
    Path(comic_id): Path<NonZeroU64>,
    Extension(api_handler): Extension<ApiHandler>,
) -> impl IntoResponse {
    let result = api_handler.get_download(comic_id).await;

    // This sse stream has the following types:
    //
    // missing: The comic was not found
    // message: All messages
    // close: The stream is instructed to close.
    match result {
        Ok(Some(stream)) => Sse::new(
            stream
                .map(|event| sse::Event::default().json_data(&event))
                .chain(tokio_stream::once(Ok(sse::Event::default()
                    .event("close")
                    .data("stream finished")))),
        )
        .into_response(),
        Ok(None) => Sse::new(
            tokio_stream::once(
                sse::Event::default()
                    .event("missing")
                    .json_data("comic not found"),
            )
            .chain(tokio_stream::once(Ok(sse::Event::default()
                .event("close")
                .data("stream finished")))),
        )
        .into_response(),
        Err(e) => (
            StatusCode::INTERNAL_SERVER_ERROR,
            Json(ApiError::from_anyhow(&e)),
        )
            .into_response(),
    }
}

async fn delete_id(
    State(collection_manager): State<Arc<CollectionManager>>,
    Path(id): Path<NonZeroU64>,
) -> impl IntoResponse {
    collection_manager
        .download
        .delete_download(id)
        .await
        .as_ref()
        .map(|deleted| match deleted {
            true => (StatusCode::OK, Json("ok")).into_response(),
            false => (StatusCode::NOT_FOUND, Json("download was not found")).into_response(),
        })
        .map_err(|error| {
            error!("{error}");

            (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(ApiError::from_anyhow(error)),
            )
        })
}
