mod comic;
mod database;
mod download;
mod tag;

use crate::routes::ApiHandler;
use crate::CollectionManager;
use axum::Extension;
use axum::Router;
use std::sync::Arc;

/// Routes for the `/api` route
pub fn routes(collection_manager: CollectionManager) -> Router {
    let api_handler = ApiHandler::new(collection_manager.clone());

    let download_routes = self::download::routes().layer(Extension(api_handler));

    Router::new()
        .nest("/download", download_routes)
        .nest("/comic", self::comic::routes())
        .nest("/tag", self::tag::routes())
        .nest("/database", self::database::routes())
        .with_state(Arc::new(collection_manager))
}
