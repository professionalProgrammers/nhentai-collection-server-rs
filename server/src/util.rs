use anyhow::Context;
use url::Url;

/// Wait for a shutdown signal
pub async fn wait_for_shutdown_signal() -> anyhow::Result<()> {
    let ctrl_c_future = tokio::signal::ctrl_c();

    #[cfg(windows)]
    let (mut ctrl_break_stream, mut ctrl_c_stream) = (
        tokio::signal::windows::ctrl_break().context("failed to register ctrl-break handler")?,
        tokio::signal::windows::ctrl_c().context("failed to register ctrl-c handler")?,
    );
    #[cfg(windows)]
    let (ctrl_break_stream_future, ctrl_c_stream_future) =
        (ctrl_break_stream.recv(), ctrl_c_stream.recv());
    #[cfg(not(windows))]
    let (ctrl_break_stream_future, ctrl_c_stream_future) = (
        std::future::pending::<Option<()>>(),
        std::future::pending::<Option<()>>(),
    );

    #[cfg(unix)]
    let mut terminate_stream =
        tokio::signal::unix::signal(tokio::signal::unix::SignalKind::terminate())
            .context("failed to register terminate handler")?;

    #[cfg(unix)]
    let terminate_future = terminate_stream.recv();

    #[cfg(not(unix))]
    let terminate_future = std::future::pending::<Option<()>>();

    tokio::select! {
        result = ctrl_c_future => {
            result.context("failed to register ctrl-c handler")?;
        },
        _result = ctrl_break_stream_future => {
            // _result is an `Option`,
            // but it returns `None` if there are no more notifications.
            // We consider this an exit path.
        },
        _result = ctrl_c_stream_future => {
            // See above.
        }
        _result = terminate_future => {
            // See above.
        }
    }

    Ok(())
}

/// Extract the file name from a [`Url`], if it exists.
pub fn extract_url_file_name(url: &Url) -> Option<&str> {
    url.path_segments().and_then(|mut iter| iter.next_back())
}
