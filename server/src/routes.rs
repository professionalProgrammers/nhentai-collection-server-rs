/// Api routes
mod api;

use crate::api_handler::ApiError;
use crate::api_handler::ApiHandler;
use crate::CollectionManager;
use axum::Router;
use camino::Utf8PathBuf;
use tower_http::services::ServeDir;
use tower_http::trace::DefaultMakeSpan;
use tower_http::trace::DefaultOnFailure;
use tower_http::trace::DefaultOnRequest;
use tower_http::trace::DefaultOnResponse;
use tower_http::trace::TraceLayer;

/// The top-level routes
pub fn routes(
    collection_path: Utf8PathBuf,
    static_assets_path: Utf8PathBuf,
    collection_manager: CollectionManager,
) -> Router {
    Router::new()
        .nest_service("/collection", ServeDir::new(collection_path))
        .nest("/api", self::api::routes(collection_manager))
        .fallback_service(ServeDir::new(static_assets_path))
        .layer(
            TraceLayer::new_for_http()
                .make_span_with(
                    DefaultMakeSpan::new()
                        .level(tracing::Level::INFO)
                        .include_headers(false),
                )
                .on_request(DefaultOnRequest::new().level(tracing::Level::INFO))
                .on_response(DefaultOnResponse::new().level(tracing::Level::INFO))
                .on_failure(DefaultOnFailure::new().level(tracing::Level::ERROR)),
        )
}
