use anyhow::bail;
use anyhow::Context;
use camino::Utf8Path;
use camino::Utf8PathBuf;
use std::net::SocketAddr;
use url::Url;

/// The config
#[derive(Debug, serde::Deserialize)]
pub struct Config {
    /// The address to bind to
    pub address: SocketAddr,

    /// TLS config
    pub tls: Option<TlsConfig>,

    /// The collection directory
    #[serde(rename = "collection-directory")]
    pub collection_directory: Utf8PathBuf,

    /// The public directory
    #[serde(rename = "public-directory")]
    pub public_directory: Utf8PathBuf,

    /// A flaresolverr server
    pub flaresolverr: Option<Url>,

    /// Cookies that should be loaded.
    #[serde(default)]
    pub cookies: Vec<Box<str>>,
}

impl Config {
    /// Load a config from the given path
    pub fn load<P>(path: P) -> anyhow::Result<Self>
    where
        P: AsRef<Utf8Path>,
    {
        let path = path.as_ref();
        let data = std::fs::read_to_string(path)
            .with_context(|| format!("failed to load config from \"{path}\""))?;
        let mut config: Self = toml::from_str(&data).context("failed to parse file")?;
        config.collection_directory = config
            .collection_directory
            .canonicalize_utf8()
            .context("failed to canonicalize collection directory")?;

        if let Some(tls_config) = config.tls.as_mut() {
            if !tls_config.self_signed {
                bail!("cannot use TLS without a self-signed certificate");
            }
        }

        Ok(config)
    }
}

#[derive(Debug, serde::Deserialize)]
pub struct TlsConfig {
    #[serde(rename = "self-signed", default)]
    pub self_signed: bool,
}
