use camino::Utf8PathBuf;

#[derive(Debug, argh::FromArgs)]
#[argh(description = "a nhentai collection server")]
pub struct CliOptions {
    #[argh(
        option,
        default = "Utf8PathBuf::from(\"./config.toml\")",
        description = "the path to the config"
    )]
    pub config: Utf8PathBuf,
}
