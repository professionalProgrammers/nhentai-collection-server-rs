mod api;
mod database;
mod download;
mod indexer;

use self::api::ApiTask;
pub use self::api::Comic as ApiComic;
pub use self::api::ComicImages as ApiComicImages;
pub use self::api::ComicPreview as ApiComicPreview;
pub use self::api::ComicSearchParams as ApiComicSearchParams;
pub use self::api::ComicTitle as ApiComicTitle;
pub use self::api::DownloadPreview as ApiDownloadPreview;
pub use self::api::Error as ApiError;
pub use self::api::Image as ApiImage;
pub use self::api::Tag as ApiTag;
pub use self::database::Comic as DatabaseComic;
pub use self::database::ComicImages as DatabaseComicImages;
use self::database::DatabaseTask;
pub use self::database::Image as DatabaseImage;
pub use self::database::ImageFormat as DatabaseImageFormat;
pub use self::database::ImageType as DatabaseImageType;
pub use self::database::Tag as DatabaseTag;
pub use self::database::TagKind as DatabaseTagKind;
pub use self::download::DownloadState;
pub use self::download::DownloadStateUpdate;
use self::download::DownloadTask;
use self::indexer::IndexerTask;
use crate::Config;
use anyhow::ensure;
use anyhow::Context;
use pikadick_util::AsyncLockFile;
use std::sync::Arc;
use tracing::error;

const METADATA_FILE_NAME: &str = "metadata.json";
const LOCK_FILE_NAME: &str = "nhentai-collection-server.lock";

/// The collection manager
#[derive(Clone)]
pub struct CollectionManager {
    lock_file: AsyncLockFile,

    /// The database
    pub database: DatabaseTask,
    pub download: DownloadTask,
    pub indexer: IndexerTask,
    pub api: ApiTask,
}

impl CollectionManager {
    /// Make a new [`CollectionManager`] with a collection at the given path.
    pub async fn new(config: Arc<Config>) -> anyhow::Result<Self> {
        let path = &config.collection_directory;

        let lock_file_path = path.join_os(LOCK_FILE_NAME);
        let lock_file = AsyncLockFile::open(lock_file_path)
            .await
            .context("failed to open lock file")?;
        let locked_lockfile = lock_file
            .try_lock_with_pid()
            .await
            .context("failed to attempt to lock the lock file")?;
        ensure!(
            locked_lockfile,
            "another process has already locked the lock file"
        );

        let database = DatabaseTask::new(path).await?;

        let download = DownloadTask::new(config.clone(), database.clone())?;

        let indexer = IndexerTask::new(database.clone(), path.into());

        let api = ApiTask::new(database.clone(), download.clone());

        Ok(Self {
            lock_file,

            database,
            download,
            indexer,
            api,
        })
    }

    /// Shutdown this collection manager
    pub async fn shutdown(&self) -> anyhow::Result<()> {
        // Shutdown the api.
        let api_result = self
            .api
            .shutdown()
            .await
            .context("failed to shutdown the api");
        if let Err(error) = api_result.as_ref() {
            error!("{error:?}");
        }

        // Shutdown the indexer.
        let indexer_result = self
            .indexer
            .shutdown()
            .await
            .context("failed to shutdown the indexer");
        if let Err(error) = indexer_result.as_ref() {
            error!("{error:?}");
        }

        // Shutdown the download task.
        //
        // While it *might* be considered critical if it fails,
        // it should persist data in a way that can be recovered.
        let download_result = self
            .download
            .shutdown()
            .await
            .context("failed to shutdown the download task");
        if let Err(error) = download_result.as_ref() {
            error!("{error:?}");
        }

        // Shutdown the database.
        let database_result = self
            .database
            .shutdown()
            .await
            .context("failed to shutdown database");
        if let Err(error) = database_result.as_ref() {
            error!("{error:?}");
        }

        // Unlock the lock file.
        let unlock_lockfile_result = self
            .lock_file
            .unlock()
            .await
            .context("failed to unlock lock file");
        if let Err(error) = unlock_lockfile_result.as_ref() {
            error!("{error:?}");
        }

        // Return errors in order of most importance.
        //
        // The most critical is the database,
        // followed by the download task,
        // followed by failing to unlock the lockfile,
        // which should never fail and will be unlocked on process close anyways.
        database_result
            .and(download_result)
            .and(indexer_result)
            .and(unlock_lockfile_result)
            .and(api_result)
    }
}
