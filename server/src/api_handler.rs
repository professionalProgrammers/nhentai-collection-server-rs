pub use crate::collection_manager::ApiError;
use crate::collection_manager::DownloadState;
use crate::collection_manager::DownloadStateUpdate;
use crate::CollectionManager;
use anyhow::anyhow;
use anyhow::Context;
use bewu_util::StateUpdateItem;
use std::num::NonZeroU64;
use tokio_stream::StreamExt;
use tracing::info;
use url::Url;

/// An event that occurs while downloading.
#[derive(Debug, serde::Deserialize, serde::Serialize)]
#[serde(tag = "type", content = "data")]
pub enum ApiDownloadEvent {
    /// The server sent the new `base state` on which to apply updates.
    NewState {
        /// This is true if the metadata was downloaded ok.
        metadata_ok: bool,

        /// The error result of fetching metadata.
        metadata_error: Option<ApiError>,

        /// urls of invalid download list entries
        invalid_download_list_entries: Vec<Url>,

        comic_dir_creaction_error: Option<ApiError>,

        /// The list of file downloads
        file_downloads: Vec<ApiDownloadEventNewStateFileDownloadEntry>,
    },

    /// Metadata was downloaded ok
    MetadataDownloadOk,

    /// Metadata failed to download
    MetadataDownloadError { error: ApiError },

    /// There was an error creating the comic list
    DownloadListCreationError { error: ApiError },

    /// The list of files that will be downloaded has been constructed
    BuiltDownloadList { files: Vec<String> },

    /// Failed to create the comic dir
    ComicDirCreationError { error: ApiError },

    /// An image was downloaded ok
    ImageDownloadOk {
        /// The file name
        file_name: Box<str>,

        /// Whether it was re-downloaded, or simply confirmed
        downloaded: bool,
    },
    /// An image failed to download
    ImageDownloadError {
        /// The file name
        file_name: Box<str>,

        /// The error
        error: ApiError,
    },
}

/// The `ApiDownloadEvent`'s `NewState` variant's `file_downloads`' entry
#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub struct ApiDownloadEventNewStateFileDownloadEntry {
    /// The file name
    file_name: Box<str>,

    /// The file state
    state: ApiDownloadEventNewStateFileDownloadEntryFileState,
}

#[derive(Debug, Copy, Clone, serde::Deserialize, serde::Serialize)]
#[serde(try_from = "u8", into = "u8")]
pub enum ApiDownloadEventNewStateFileDownloadEntryFileState {
    Started = 1,
    Downloaded = 2,
    Verified = 3,
    Error = 4,
}

impl TryFrom<u8> for ApiDownloadEventNewStateFileDownloadEntryFileState {
    type Error = u8;

    fn try_from(n: u8) -> Result<Self, u8> {
        match n {
            1 => Ok(Self::Started),
            2 => Ok(Self::Downloaded),
            3 => Ok(Self::Verified),
            4 => Ok(Self::Error),
            _ => Err(n),
        }
    }
}

impl From<ApiDownloadEventNewStateFileDownloadEntryFileState> for u8 {
    fn from(v: ApiDownloadEventNewStateFileDownloadEntryFileState) -> Self {
        v as u8
    }
}

impl ApiDownloadEvent {
    /// Make a `NewState` event
    fn create_new_state_event(state: &DownloadState) -> Self {
        let state = state.lock();

        let metadata_result = state.get_metadata_result();
        let file_downloads_map = state.get_file_downloads();

        let mut file_downloads = Vec::with_capacity(file_downloads_map.len());
        for (file_name, data) in file_downloads_map.iter() {
            file_downloads.push(ApiDownloadEventNewStateFileDownloadEntry {
                file_name: file_name.clone(),
                state: data.as_ref().map_or(
                    ApiDownloadEventNewStateFileDownloadEntryFileState::Started,
                    |data| match data {
                        Ok(true) => ApiDownloadEventNewStateFileDownloadEntryFileState::Downloaded,
                        Ok(false) => ApiDownloadEventNewStateFileDownloadEntryFileState::Verified,
                        Err(_e) => ApiDownloadEventNewStateFileDownloadEntryFileState::Error,
                    },
                ),
            });
        }
        file_downloads.sort_unstable_by(|entry1, entry2| {
            let maybe_n_1 = entry1
                .file_name
                .split_once('.')
                .and_then(|t| t.0.parse::<u32>().ok());
            let maybe_n_2 = entry2
                .file_name
                .split_once('.')
                .and_then(|t| t.0.parse::<u32>().ok());
            match (maybe_n_1, maybe_n_2) {
                (Some(entry1), Some(entry2)) => entry1.cmp(&entry2),
                (Some(_entry1), None) => std::cmp::Ordering::Less,
                (None, Some(_entry2)) => std::cmp::Ordering::Greater,
                (None, None) => entry1.file_name.cmp(&entry2.file_name),
            }
        });

        Self::NewState {
            metadata_ok: metadata_result
                .as_ref()
                .is_some_and(|result| result.is_ok()),
            metadata_error: metadata_result
                .and_then(|result| result.clone().map_err(|error| ApiError::from(&error)).err()),
            invalid_download_list_entries: state.get_invalid_download_list_entries().to_vec(),
            comic_dir_creaction_error: state
                .get_comic_dir_creation_error()
                .cloned()
                .map(|error| ApiError::from(&error)),

            file_downloads,
        }
    }

    /// Make an event from a download state update
    fn from_download_state_update(update: DownloadStateUpdate) -> Self {
        match update {
            DownloadStateUpdate::MetadataDownloaded { result } => match result {
                Ok(_metadata) => Self::MetadataDownloadOk,
                Err(error) => Self::MetadataDownloadError {
                    error: ApiError::from(&error),
                },
            },
            DownloadStateUpdate::InvalidDownloadListEntry { url } => {
                Self::DownloadListCreationError {
                    error: ApiError::from_anyhow(&anyhow!(
                        "failed to create download list entry for `{url}`"
                    )),
                }
            }
            DownloadStateUpdate::DownloadListGenerated { download_list } => {
                let files: Vec<_> = download_list.iter().map(|el| el.0.to_string()).collect();
                Self::BuiltDownloadList { files }
            }
            DownloadStateUpdate::CreateComicDirFail { error } => Self::ComicDirCreationError {
                error: ApiError::from(&error),
            },
            DownloadStateUpdate::FileSaved { file_name, result } => match result {
                Ok(downloaded) => ApiDownloadEvent::ImageDownloadOk {
                    file_name,
                    downloaded,
                },
                Err(error) => ApiDownloadEvent::ImageDownloadError {
                    file_name,
                    error: ApiError::from(&error),
                },
            },
        }
    }
}

/// A wrapper for a collection manager specialized for responding to api requests
#[derive(Clone)]
pub struct ApiHandler {
    /// The inner CollectionManager
    pub collection_manager: CollectionManager,
}

impl ApiHandler {
    /// Make a new [`ApiHandler`]
    pub fn new(collection_manager: CollectionManager) -> Self {
        Self { collection_manager }
    }

    /// Get a download, aka subscribe to a download stream
    pub async fn get_download(
        &self,
        comic_id: NonZeroU64,
    ) -> anyhow::Result<Option<impl tokio_stream::Stream<Item = ApiDownloadEvent>>> {
        info!("subscribing to download of comic \"{comic_id}\"");

        let state_pair = match self
            .collection_manager
            .download
            .subscribe_download(comic_id)
            .await
            .context("failed to subscribe to download")?
        {
            Some(state_pair) => state_pair,
            None => return Ok(None),
        };
        let stream = state_pair.into_stream();

        let stream = stream.map(move |update| match update {
            StateUpdateItem::Update(update) => ApiDownloadEvent::from_download_state_update(update),
            StateUpdateItem::State(state) => {
                // Stream lagged, messages lost, re-send current state.
                ApiDownloadEvent::create_new_state_event(&state)
            }
        });

        Ok(Some(stream))
    }
}
