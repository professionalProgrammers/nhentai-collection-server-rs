INSERT OR REPLACE INTO comics (
    id, 
    pretty_title, 
    english_title,
    japanese_title,
    num_favorites,
    upload_date
) VALUES (
    :id, 
    :pretty_title, 
    :english_title,
    :japanese_title,
    :num_favorites,
    :upload_date
);