SELECT
    id,
    pretty_title,
    english_title,
    japanese_title,
    num_favorites,
    upload_date
FROM
    comics
WHERE
    id = :id;