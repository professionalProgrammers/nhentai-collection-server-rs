PRAGMA page_size = 4096;
PRAGMA journal_mode = WAL;
PRAGMA foreign_keys = ON;
PRAGMA synchronous = NORMAL;

CREATE TABLE IF NOT EXISTS comics (
    id INTEGER PRIMARY KEY NOT NULL UNIQUE,
    pretty_title TEXT NOT NULL,
    english_title TEXT NOT NULL,
    japanese_title TEXT NULL,
    num_favorites INTEGER NOT NULL,
    upload_date INTEGER NOT NULL
) STRICT;

CREATE TABLE IF NOT EXISTS images (
    comic_id INTEGER NOT NULL,
    image_type TEXT NOT NULL CHECK (image_type IN ('cover', 'thumbnail', 'page')),
    width INTEGER NOT NULL,
    height INTEGER NOT NULL,
    extension TEXT NOT NULL,
    
    -- if page_number > 0, it is the page index starting at 1
    -- else, this field is meaningless and defaulted to 0.
    -- 
    -- this field is defaulted to 0 instead of NULL 
    -- as sqlite has a limitation where a NULL key cannot be a part of a UNIQUE constraint
    page_number INTEGER NOT NULL DEFAULT 0,
    
    FOREIGN KEY (comic_id) REFERENCES comics (id),
    PRIMARY KEY (comic_id, image_type, page_number),
    UNIQUE (comic_id, image_type, page_number)
) STRICT;

CREATE TABLE IF NOT EXISTS tags (
    id INTEGER PRIMARY KEY NOT NULL,
    name TEXT NOT NULL,
    type TEXT NOT NULL,
    
    UNIQUE (name, type)
) STRICT;

CREATE TABLE IF NOT EXISTS comic_tags (
    comic_id INTEGER NOT NULL,
    tag_id INTEGER NOT NULL,

    FOREIGN KEY (comic_id) REFERENCES comics (id),
    FOREIGN KEY (tag_id) REFERENCES tags (id),
    UNIQUE (comic_id, tag_id),
    PRIMARY KEY (comic_id, tag_id)
) STRICT;

CREATE INDEX IF NOT EXISTS comics_id_index ON comics (id);
CREATE INDEX IF NOT EXISTS images_comic_id_index ON images (comic_id);

CREATE UNIQUE INDEX IF NOT EXISTS
    images_index 
ON 
    images (
        comic_id, 
        image_type
    ) 
WHERE 
    image_type IN ('cover', 'thumbnail');