INSERT OR REPLACE INTO images (
    comic_id, 
    image_type, 
    width, 
    height, 
    extension, 
    page_number
) VALUES (
    :comic_id, 
    :image_type, 
    :width, 
    :height, 
    :extension, 
    :page_number
);