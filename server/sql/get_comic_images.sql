SELECT 
    image_type, 
    width, 
    height, 
    extension 
FROM 
    images
WHERE 
    comic_id = :id 
ORDER BY 
    page_number;