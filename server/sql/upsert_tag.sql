INSERT OR REPLACE INTO tags (
    id, 
    name, 
    type
) VALUES (
    :id, 
    :name, 
    :type
);