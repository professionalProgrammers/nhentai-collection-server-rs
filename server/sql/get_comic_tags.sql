SELECT 
    id, 
    name, 
    type
FROM 
    tags 
JOIN 
    comic_tags
ON
    comic_tags.tag_id = tags.id
WHERE
    comic_id = :id
ORDER BY 
    name;