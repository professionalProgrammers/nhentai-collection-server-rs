DROP INDEX IF EXISTS comics_id_index;
DROP INDEX IF EXISTS images_comic_id_index;
DROP INDEX IF EXISTS images_index;

DROP TABLE IF EXISTS comic_tags;
DROP TABLE IF EXISTS tags;
DROP TABLE IF EXISTS images;
DROP TABLE IF EXISTS comics;