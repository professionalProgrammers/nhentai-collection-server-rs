import { restoreScroll } from "svelte-spa-router";

export function updateScroll() {
  let historyState = window.history.state || {};
  let scrollX = window.scrollX;
  let scrollY = window.scrollY;
  window.history.replaceState(
    {
      __svelte_spa_router_scrollX: scrollX,
      __svelte_spa_router_scrollY: scrollY,
    },
    "",
  );

  // console.log(`Saving scroll as ${scrollX} x ${scrollY}`);
}

// I honestly cannot believe how fucking handicapped svelte stores are.
// Getting the value of a writable store is simply impossible,
// so we have to go full fucking circle and use getters/setters.
// Honestly fucking incredible.
export let shouldUpdateScroll = true;
export function setShouldUpdateScroll(value) {
  shouldUpdateScroll = value;
}
