class Api {
  constructor() {
    this.cache = {};
    this.cache.getComic = {};
    this.cache.listTags = null;
  }

  /// Clear the cache
  clearCache() {
    this.cache = {};
  }

  /// List comics
  async listComics({ query, tags } = {}) {
    let params = new URLSearchParams();
    if (query !== null && query !== undefined && query !== "")
      params.set("query", query);
    if (tags !== null && tags !== undefined && tags.size !== 0)
      params.set("tags", Array.from(tags).join(","));
    params = params.toString();
    if (params.length !== 0) params = "?" + params;

    let response = await fetch(`/api/comic${params}`);
    let json = await response.json();
    if (response.ok) {
      return json;
    } else {
      throw json;
    }
  }

  /// Get a comic by id
  async getComic(comicId) {
    comicId = Number(comicId);
    if (!Number.isInteger(comicId))
      throw new Error("`comicId` is not an integer");

    if (this.cache.getComic[comicId]) return this.cache.getComic[comicId];

    let response = await fetch(`/api/comic/${comicId}`);
    let json = await response.json();
    if (response.ok) {
      this.cache.getComic[comicId] = json;
      return json;
    } else {
      throw json;
    }
  }

  /// List tags
  async listTags() {
    if (this.cache.listTags) return this.cache.listTags;

    let response = await fetch(`/api/tag`);
    let json = await response.json();
    if (response.ok) {
      return json;
    } else {
      throw json;
    }
  }

  /// List downloads
  async listDownloads() {
    let response = await fetch(`/api/download`);
    let json = await response.json();
    if (response.ok) {
      return json;
    } else {
      throw json;
    }
  }

  /// Create a download by id
  async createDownload(comic_id) {
    comic_id = parseInt(comic_id);
    if (!Number.isInteger(comic_id))
      throw new Error("`comic_id` is not an integer");

    let response = await fetch(`/api/download/${comic_id}`, {
      method: "POST",
    });
    let json = await response.json();
    if (response.ok) {
      return json;
    } else {
      throw json;
    }
  }

  /// Get a download by id
  async *getDownload(comic_id) {
    comic_id = parseInt(comic_id);
    if (!Number.isInteger(comic_id))
      throw new Error("`comic_id` is not an integer");

    let shouldExit = false;
    let resolveFunc = null;
    let rejectFunc = null;

    let sse = new EventSource(`/api/download/${comic_id}`);
    sse.addEventListener("error", (event) => {
      console.error(event);

      // TODO: Make downloads restartable
      sse.close();
      shouldExit = true;

      rejectFunc({
        type: "error",
        data: event,
      });
    });
    sse.addEventListener("message", (event) => {
      resolveFunc(JSON.parse(event.data));
    });
    sse.addEventListener("missing", (event) => {
      // The user asked to subscribe to something that doesn't exist.
      // All we can do is exit.
      sse.close();
      shouldExit = true;

      rejectFunc({
        type: "missing",
        data: JSON.parse(event.data),
      });
    });
    sse.addEventListener("close", (event) => {
      // Since the server said it was done sending updates, we disconnect.
      sse.close();
      shouldExit = true;

      resolveFunc({
        type: "close",
      });
    });

    while (!shouldExit) {
      let nextEventPromise = new Promise((resolve, reject) => {
        resolveFunc = resolve;
        rejectFunc = reject;
      });

      yield await nextEventPromise;
    }
  }

  /// Delete a download by id
  async deleteDownload(comic_id) {
    comic_id = parseInt(comic_id);
    if (!Number.isInteger(comic_id))
      throw new Error("`comic_id` is not an integer");

    let response = await fetch(`/api/download/${comic_id}`, {
      method: "DELETE",
    });
    let json = await response.json();
    if (response.ok) {
      return json;
    } else {
      throw json;
    }
  }

  async purgeDatabase() {
    let response = await fetch(`/api/database/purge`);
    let json = await response.json();
    if (response.ok) {
      return json;
    } else {
      throw json;
    }
  }

  async optimizeDatabase() {
    let response = await fetch(`/api/database/optimize`);
    let json = await response.json();

    if (response.ok) {
      return json;
    } else {
      throw json;
    }
  }
}

let api = new Api();
window.api = api;
export default api;
